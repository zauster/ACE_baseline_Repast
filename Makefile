#***************************************************************************
#
# Repast HPC Tutorial Makefile
#
#***************************************************************************

include ./env

all : bin/blmodel

.PHONY: clean_all
clean_all:
	rm -f *.csv
	rm -f *.txt
	rm -f output/*.csv
	rm -f output/*.txt
	rm -f logs/*.*

.PHONY: clean
clean:
	rm -f *.o
	rm -f objects/*.o

PREFIX= /usr/bin/
# PREFIX= /opt/mpich/bin/
DVAR=
MPICXX= $(PREP) $(PREFIX)mpicxx -std=c++11
SRCDIR = src
OBJDIR = objects
SRCFILES = $(wildcard $(SRCDIR)/*.cpp)
OBJECTS = $(patsubst $(SRCDIR)/%.cpp, $(OBJDIR)/%.o, $(SRCFILES))

bin/blmodel: $(OBJECTS)
	$(MPICXX) $(BOOST_LIB_DIR) $(REPAST_HPC_LIB_DIR) $^ -o $@ $(BOOST_LIBS) $(REPAST_HPC_LIB)

$(OBJECTS): $(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(MPICXX) $(DVAR) $(BOOST_INCLUDE) $(REPAST_HPC_INCLUDE) -Wno-deprecated -I./include -c  $< -o $@

test: bin/blmodel
	rm -f output/log*
	$(PREFIX)mpirun -np 1 bin/blmodel props/config.props props/model.props
	@printf "\n\n"
	rm -f output/log*
	$(PREFIX)mpirun -np 2 bin/blmodel props/config.props props/model.props

# mpirun -np 16 bin/blmodel props/config.props props/model.props
# Demo 0 -> fine
# Demo 1 -> fine
# Demo 2 -> fine
# Demo 3:
#  - step 01 - step 06: error in execution when n.threads != 4. that line
#  - step 07: error in compilation
#  - step 08: error in execution when n.threads != 4
