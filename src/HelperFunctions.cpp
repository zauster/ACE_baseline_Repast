#include "HelperFunctions.hpp"

std::vector<std::string> getNextLineAndSplitIntoTokens(std::istream& str,
                                                       char split = ',') {
  std::vector<std::string> result;
  std::string line;
  std::getline(str, line);
  std::stringstream lineStream(line);
  std::string cell;
  while(std::getline(lineStream, cell, split)) {
    result.push_back(cell);
  }
  // This checks for a trailing comma with no data after it.
  if (!lineStream && cell.empty()) {
    // If there was a trailing comma then add an empty element.
    result.push_back("");
  }
  return result;
}

int getRandInt(int lower, int upper) {
  static std::random_device rd;
  static std::mt19937 gen(rd());
  // static std::mt19937 gen(1234);
  std::uniform_int_distribution<> dis(lower, upper);
  return dis(gen);

  // repast::IntUniformGenerator rnd = repast::Random::instance()->createUniIntGenerator(lower, upper);
  // rnd.next();
}


int getRandIndex(int nrElements) {
  static std::random_device rd;
  static std::mt19937 gen(rd());
  // static std::mt19937 gen(123);
  std::uniform_int_distribution<> dis(0, nrElements - 1);
  return dis(gen);

  // repast::IntUniformGenerator rnd = repast::Random::instance()->createUniIntGenerator(0, nrElements - 1);
  // rnd.next();
}

double getRandDouble(double lower, double upper) {
  // static std::random_device rd;
  // static std::mt19937 gen(rd());
  // std::uniform_real_distribution<> dis(lower, upper);
  // return dis(gen);

  repast::DoubleUniformGenerator dug = repast::Random::instance()->createUniDoubleGenerator(lower, upper);
  dug.next();
}
