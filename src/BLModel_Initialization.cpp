#include <stdio.h>
#include <vector>
#include <boost/mpi.hpp>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/initialize_random.h"
// #include "repast_hpc/SVDataSetBuilder.h"

#include "BLModel.hpp"
#include "HelperFunctions.hpp"

BOOST_CLASS_EXPORT_GUID(repast::SpecializedProjectionInfoPacket<repast::RepastEdgeContent<repast::Agent> >, "SpecializedProjectionInfoPacket_EDGE");


BLModel::BLModel(std::string propsFile, int argc, char** argv,
                 boost::mpi::communicator* comm): context(comm){
  props = new repast::Properties(propsFile, argc, argv, comm);
  // countOfAgents = repast::strToInt(props->getProperty("count.of.agents"));

  stopAt = repast::strToInt(props->getProperty("stopAt"));
  NrFirms = repast::strToInt(props->getProperty("NrFirms"));
  NrConsumers = repast::strToInt(props->getProperty("NrConsumers"));
  maxTypeA = repast::strToInt(props->getProperty("maxTypeA"));
  lowerBoundInventories = repast::strToDouble(props->getProperty("lowerBoundInventories"));
  upperBoundInventories = repast::strToDouble(props->getProperty("upperBoundInventories"));
  lowerBoundPrice = repast::strToDouble(props->getProperty("lowerBoundPrice"));
  upperBoundPrice = repast::strToDouble(props->getProperty("upperBoundPrice"));
  ProbNewPrice = repast::strToDouble(props->getProperty("ProbNewPrice"));
  gamma = repast::strToInt(props->getProperty("gamma"));
  delta = repast::strToDouble(props->getProperty("delta"));
  theta = repast::strToDouble(props->getProperty("theta"));
  pi = repast::strToDouble(props->getProperty("pi"));
  epsilon = repast::strToDouble(props->getProperty("epsilon"));
  maxNrFirms_openVacancy = repast::strToInt(props->getProperty("maxNrFirms_openVacancy"));
  maxNrFirms_buyGoods = repast::strToInt(props->getProperty("maxNrFirms_buyGoods"));
  NrFirms_PriceSwitch = repast::strToInt(props->getProperty("NrFirms_PriceSwitch"));
  LabourProductivity = repast::strToDouble(props->getProperty("LabourProductivity"));

  worldSize = repast::RepastProcess::instance()->worldSize();

  initializeRandom(*props, comm);
  if(repast::RepastProcess::instance()->rank() == 0) {
    props->writeToSVFile("./output/record.csv");
  }

  // Initialize the handler for the agent packages
  agent_provider = new AgentPackageProvider(&context);
  agent_receiver = new AgentPackageReceiver(&context);



  LabourMarketNetwork = new repast::SharedNetwork<repast::Agent,
                                                  repast::RepastEdge<repast::Agent>,
                                                  repast::RepastEdgeContent<repast::Agent>,
                                                  repast::RepastEdgeContentManager<repast::Agent> >("LabourMarketNetwork", false, &edgeContentManager);
  context.addProjection(LabourMarketNetwork);
  LabourSearchNetwork = new repast::SharedNetwork<repast::Agent,
                                                  repast::RepastEdge<repast::Agent>,
                                                  repast::RepastEdgeContent<repast::Agent>,
                                                  repast::RepastEdgeContentManager<repast::Agent> >("LabourSearchNetwork", false, &edgeContentManager);
  context.addProjection(LabourSearchNetwork);
  JobOfferNetwork = new repast::SharedNetwork<repast::Agent,
                                              repast::RepastEdge<repast::Agent>,
                                              repast::RepastEdgeContent<repast::Agent>,
                                              repast::RepastEdgeContentManager<repast::Agent> >("JobOfferNetwork", false, &edgeContentManager);
  context.addProjection(JobOfferNetwork);
  AcceptedJobNetwork = new repast::SharedNetwork<repast::Agent,
                                              repast::RepastEdge<repast::Agent>,
                                              repast::RepastEdgeContent<repast::Agent>,
                                              repast::RepastEdgeContentManager<repast::Agent> >("AcceptedJobNetwork", false, &edgeContentManager);
  context.addProjection(AcceptedJobNetwork);
  LeaveJobNetwork = new repast::SharedNetwork<repast::Agent,
                                                 repast::RepastEdge<repast::Agent>,
                                                 repast::RepastEdgeContent<repast::Agent>,
                                                 repast::RepastEdgeContentManager<repast::Agent> >("LeaveJobNetwork", false, &edgeContentManager);
  context.addProjection(LeaveJobNetwork);


  // Goods Stuff
  GoodsMarketNetwork = new repast::SharedNetwork<repast::Agent,
                                                 repast::RepastEdge<repast::Agent>,
                                                 repast::RepastEdgeContent<repast::Agent>,
                                                 repast::RepastEdgeContentManager<repast::Agent> >("GoodsMarketNetwork", false, &edgeContentManager);
  context.addProjection(GoodsMarketNetwork);
  GoodsDeliveryNetwork = new repast::SharedNetwork<repast::Agent,
                                                   repast::RepastEdge<repast::Agent>,
                                                   repast::RepastEdgeContent<repast::Agent>,
                                                   repast::RepastEdgeContentManager<repast::Agent> >("GoodsDeliveryNetwork", false, &edgeContentManager);
  context.addProjection(GoodsDeliveryNetwork);


  // Payment stuff
  WagePaymentNetwork = new repast::SharedNetwork<repast::Agent,
                                                 repast::RepastEdge<repast::Agent>,
                                                 repast::RepastEdgeContent<repast::Agent>,
                                                 repast::RepastEdgeContentManager<repast::Agent> >("WagePaymentNetwork", false, &edgeContentManager);
  context.addProjection(WagePaymentNetwork);
  DividendPaymentNetwork = new repast::SharedNetwork<repast::Agent,
                                                     repast::RepastEdge<repast::Agent>,
                                                     repast::RepastEdgeContent<repast::Agent>,
                                                     repast::RepastEdgeContentManager<repast::Agent> >("DividendPaymentNetwork", false, &edgeContentManager);
  context.addProjection(DividendPaymentNetwork);

}

BLModel::~BLModel(){
  delete props;
  delete agent_provider;
  delete agent_receiver;
}

void BLModel::initStates(){
  int myRank = repast::RepastProcess::instance()->rank();
  int moveToRank;
  int NrProc = repast::RepastProcess::instance()->worldSize();

  //
  // Firm initial data
  //
  std::ifstream initFirm("input/firms.csv");
  std::vector<std::string> colNames;
  std::vector<std::string> tmp;
  repast::AgentRequest request(myRank);
  colNames = getNextLineAndSplitIntoTokens(initFirm, ',');
  tmp = getNextLineAndSplitIntoTokens(initFirm, ',');
  int j = 1;
  while(initFirm) {
    // evenly distribute the agents among the processes
    // for two procs: even IDs go to rank 0, odd IDs to rank 1
    moveToRank = j % NrProc;

    if(moveToRank == myRank) {

      std::string line1;
      std::istringstream str1(tmp[16]);
      std::getline(str1, line1);
      std::istringstream rowInputStream1(line1);
      std::vector<int> tmpEmployeeList;
      std::string cell;

      // std::cout << "Firm " << j << ": ";
      while(std::getline(rowInputStream1, cell, ':')) {
        // std::cout << cell << ", ";
        tmpEmployeeList.push_back(repast::strToInt(cell));
      }
      // std::cout << std::endl;

      // this is a local firm, so we instantiate it here
      repast::AgentId tmpID(j, moveToRank, 1);
      Firm* firm = new Firm(tmpID, // j, // Firm_ID
                            repast::strToDouble(tmp[2]), // Firm_Liquidity
                            repast::strToDouble(tmp[3]), // marginalCosts
                            (bool) repast::strToInt(tmp[4]), // openVacancy
                            (bool) repast::strToInt(tmp[5]), // FireAnEmployee
                            repast::strToInt(tmp[6]), // OutstandingJobOffers
                            // openVacancy_lastMonth
                            (bool) repast::strToInt(tmp[7]),
                            // MonthsSinceLastVacancy
                            repast::strToInt(tmp[8]),
                            repast::strToDouble(tmp[9]), // currentWage
                            repast::strToInt(tmp[10]), // NrEmployees
                            // currentInventories
                            repast::strToDouble(tmp[11]),
                            repast::strToDouble(tmp[12]), // currentDemand
                            repast::strToDouble(tmp[15]),
                            tmpEmployeeList); // currentPrice

      // std::cout << myRank << ": " << j << " = " << firm->getId()
      //           << std::endl;
      context.addAgent(firm);
    } else {

      // this firm is non-local. I.e., add it to our agent request
      // because we will need all firms to build the graph edges
      repast::AgentId tmpID(j, moveToRank, 1);
      request.addRequest(tmpID);

    }
    tmp = getNextLineAndSplitIntoTokens(initFirm, ',');
    j++;
  }

  // here we have correctly instantiated all local firm agents. To
  // instantiate the consumers and set the graph network, we will need
  // ALL (also non-local) firms. Thus we request them here.
  repast::RepastProcess::instance()->requestAgents<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, request, *agent_provider, *agent_receiver, *agent_receiver);

  //
  // Consumer initial data
  //
  std::ifstream initConsumer("input/consumer.csv");
  colNames = getNextLineAndSplitIntoTokens(initConsumer, ',');
  tmp = getNextLineAndSplitIntoTokens(initConsumer, ',');
  j = 1;
  while(initConsumer) {
    moveToRank = j % NrProc;

    if(moveToRank == myRank) {
      std::string line1;
      std::istringstream str1(tmp[12]);
      std::getline(str1, line1);
      std::istringstream rowInputStream1(line1);
      std::vector<int> tmpMySellerList;
      std::map<int, double> tmpMySellerList_currPrices;
      std::string cell;

      // std::cout << "Cons " << j << ": ";
      while(std::getline(rowInputStream1, cell, ':')) {
        // std::cout << cell << ", ";
        tmpMySellerList.push_back(repast::strToInt(cell));
        tmpMySellerList_currPrices.insert(std::pair<int, double>(repast::strToInt(cell), 1.1));
      }
      // std::cout << std::endl;

      repast::AgentId tmpID(j, myRank, 2);
      Cons* cons = new Cons(tmpID, // Cons_ID
                            (bool) repast::strToInt(tmp[2]), // isUnemployed
                            repast::strToInt(tmp[3]), // EmployerID
                            repast::strToDouble(tmp[4]), // Cons_Liquidity
                            repast::strToDouble(tmp[5]), // ReservationWage
                            repast::strToDouble(tmp[6]), // ReceivedWage
                            repast::strToDouble(tmp[7]), // ReceivedDividend
                            repast::strToDouble(tmp[8]), // plannedDemand
                            0.0, // dailyDemand
                            repast::strToDouble(tmp[9]), // realizedDemand
                            // plannedDemand_lastMonth
                            repast::strToDouble(tmp[10]),
                            // unsatisfiedDemand
                            repast::strToDouble(tmp[11]),
                            // tmpMySellerList,
                            tmpMySellerList_currPrices
                            );
      // std::cout << myRank << ": " << j << " = " << cons->getId()
      //           << std::endl;
      context.addAgent(cons);


      // Make the GoodsMarketNetwork connection
      Firm* tmpFirm;
      for(std::vector<int>::iterator seller_it = tmpMySellerList.begin();
          seller_it != tmpMySellerList.end(); seller_it++) {
        // we know that firms with even IDs are on rank 0 and odd IDs on
        // rank 1
        int FirmRank = *seller_it % NrProc;
        repast::AgentId tmpID(*seller_it, FirmRank, 1);
        tmpFirm = dynamic_cast<Firm *> (context.getAgent(tmpID));
        GoodsMarketNetwork->addEdge(cons, tmpFirm, 0.0);
      }

      // Make the Accepted Job Network connection
      int tmpEmployerID = cons->getEmployerID();
      if(tmpEmployerID != 0) {
        repast::AgentId tmpID(tmpEmployerID, tmpEmployerID % NrProc, 1);
        tmpFirm = dynamic_cast<Firm *>(context.getAgent(tmpID));
        AcceptedJobNetwork->addEdge(cons, tmpFirm);
      }
    }

    tmp = getNextLineAndSplitIntoTokens(initConsumer, ',');
    j++;
  } // end of for loop reading in consumer initial data

  // std::cout << " -> 0. Context size on rank " << myRank
  //           << ": " << context.size()
  //           << " LM: " << LabourMarketNetwork->edgeCount()
  //           << " AM: " << AcceptedJobNetwork->edgeCount()
  //           << " GM: " << GoodsMarketNetwork->edgeCount()
  //           << " SM: " << LabourSearchNetwork->edgeCount()
  //           << " JM: " << JobOfferNetwork->edgeCount()
  //           << " WM: " << WagePaymentNetwork->edgeCount()
  //           << " DM: " << DividendPaymentNetwork->edgeCount()
  //           << std::endl;

  // std::cout << myRank << ": Balancing the projection" << std::endl;
  LabourMarketNetwork->balance();
  GoodsMarketNetwork->balance();
  GoodsDeliveryNetwork->balance();

  // std::cout << myRank << ": Synchronizing agents\n";
  repast::RepastProcess::instance()->synchronizeAgentStatus<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);
  repast::RepastProcess::instance()->synchronizeProjectionInfo<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);
  repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*agent_provider, *agent_receiver);

  std::set<repast::Agent*> firms;
  std::vector<int> tmpEmp;
  Cons* tmpCons;
  Firm* tmpFirm;
  context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
                       firms, 1);
  for(std::set<repast::Agent*>::iterator firm_it = firms.begin();
      firm_it != firms.end(); firm_it++) {
    tmpFirm = dynamic_cast<Firm*> (*firm_it);
    tmpEmp = tmpFirm->getEmployeeList();
    for(int i = 0; i < tmpEmp.size(); i++) {
      repast::AgentId tmpID(tmpEmp[i], tmpEmp[i] % NrProc, 2);
      tmpCons = dynamic_cast<Cons *>(context.getAgent(tmpID));
      LabourMarketNetwork->addEdge(tmpFirm, tmpCons);
    }
  }

  repast::RepastProcess::instance()->synchronizeProjectionInfo<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);

  // std::cout << " -> 1. Context size on rank " << myRank
  //           << ": " << context.size()
  //           << " LM: " << LabourMarketNetwork->edgeCount()
  //           << " AM: " << AcceptedJobNetwork->edgeCount()
  //           << " GM: " << GoodsMarketNetwork->edgeCount()
  //           << " SM: " << LabourSearchNetwork->edgeCount()
  //           << " JM: " << JobOfferNetwork->edgeCount()
  //           << " WM: " << WagePaymentNetwork->edgeCount()
  //           << " DM: " << DividendPaymentNetwork->edgeCount()
  //           << std::endl;

  // //
  // // ----------------------------------------
  // // control for correctness of Goods Network
  // // from view of consumers
  // std::set<repast::Agent*> consumers;
  // context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
  //                      consumers, 2);
  // std::vector<repast::Agent*> tmpVec;
  // std::cout << std::endl << " ------ "
  //           << myRank << ": correctness ------------\n";
  // for(std::set<repast::Agent*>::iterator it = consumers.begin();
  //     it != consumers.end(); it++) {
  //   std::cout << " -> Check " << (*it)->getId() << ": ";
  //   // get adjacent neighbouring firms
  //   GoodsMarketNetwork->adjacent(*it, tmpVec);
  //   for(std::vector<repast::Agent*>::iterator tmpIt = tmpVec.begin();
  //       tmpIt != tmpVec.end(); tmpIt++) {
  //     std::cout << " > " << (*tmpIt)->getId().id();
  //   }
  //   std::cout << std::endl;
  //   tmpVec.clear();
  // }
  // // Result: from the consumer's perspective, all links to his/her
  // // firms are set correctly

  // // control for correctness of Labour Network
  // tmpVec.clear();
  // for(std::set<repast::Agent*>::iterator it = consumers.begin();
  //     it != consumers.end(); it++) {
  //   std::cout << " -> Check labour " << (*it)->getId() << ": ";
  //   // get adjacent neighbouring firms
  //   LabourMarketNetwork->adjacent(*it, tmpVec);
  //   for(std::vector<repast::Agent*>::iterator tmpIt = tmpVec.begin();
  //       tmpIt != tmpVec.end(); tmpIt++) {
  //     std::cout << " > " << (*tmpIt)->getId().id();
  //   }
  //   std::cout << std::endl;
  //   tmpVec.clear();
  // }
  // // Result: from the consumer's perspective, the link to his/her
  // // employer is set correctly

  // // check correctness of labour network from the firms perspective
  // std::set<repast::Agent*> firms;
  // context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
  //                      firms, 1);
  // std::vector<repast::Agent*> worker;
  // for(std::set<repast::Agent*>::iterator it = firms.begin();
  //     it != firms.end(); it++) {
  //   LabourMarketNetwork->adjacent(*it, worker);
  //   std::cout << " firm " << (*it)->getId() << " - " << worker.size()
  //             << " - {";
  //   for(std::vector<repast::Agent*>::iterator jt = worker.begin();
  //       jt != worker.end(); jt++) {
  //     std::cout << (*jt)->getId() << ", ";
  //   }
  //   std::cout << "}" << std::endl;
  //   worker.clear();
  // }

}

void BLModel::initSchedule(repast::ScheduleRunner& runner){

  // Beginning of a month, update the employment situation and Trading
  // Partners Update
  runner.scheduleEvent(1, 20, repast::Schedule::FunctorPtr(new repast::MethodFunctor<BLModel> (this, &BLModel::doEmploymentUpdate)));
  runner.scheduleEvent(1.1, 20, repast::Schedule::FunctorPtr(new repast::MethodFunctor<BLModel> (this, &BLModel::doConsumerPricesUpdate)));
  runner.scheduleEvent(1.2, 20, repast::Schedule::FunctorPtr(new repast::MethodFunctor<BLModel> (this, &BLModel::doFirmsPricesUpdate)));

  // Daily routines
  runner.scheduleEvent(1.5, 1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<BLModel> (this, &BLModel::FirmsProduceGoods)));
  runner.scheduleEvent(1.6, 1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<BLModel> (this, &BLModel::ConsumerConsumeGoods)));

  // Paytime
  runner.scheduleEvent(20.7, 20, repast::Schedule::FunctorPtr(new repast::MethodFunctor<BLModel> (this, &BLModel::doWagePayments)));

  // write the states of the agents
  runner.scheduleEvent(1.8, 20, repast::Schedule::FunctorPtr(new repast::MethodFunctor<BLModel> (this, &BLModel::writeAgentStates)));
  // stop here
  runner.scheduleStop(stopAt);
}

void BLModel::updateProjection() {
  // int myRank = repast::RepastProcess::instance()->rank();

  // // std::cout << " => Synchronizing agents ...\n";
  // repast::RepastProcess::instance()->synchronizeAgentStatus<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);
  // repast::RepastProcess::instance()->synchronizeProjectionInfo<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);
  // repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*agent_provider, *agent_receiver);

}
