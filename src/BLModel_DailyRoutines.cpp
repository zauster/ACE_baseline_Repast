#include <stdio.h>
#include <vector>
#include <boost/mpi.hpp>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/initialize_random.h"

#include "BLModel.hpp"
#include "HelperFunctions.hpp"


// firm produce goods
void BLModel::FirmsProduceGoods() {
  int myRank = repast::RepastProcess::instance()->rank();
  int worldSize = repast::RepastProcess::instance()->worldSize();

  // std::cout << myRank << ": Firms producing output at "
  //           << repast::RepastProcess::instance()->getScheduleRunner().currentTick()
  //           << std::endl;

  // repast::SharedContext<repast::Agent>::const_state_aware_bytype_iterator it;
  // it = context.byTypeBegin(repast::SharedContext<repast::Agent>::LOCAL, 1);

  std::set<repast::Agent*> firms;
  Firm* tmpFirm;
  context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
                       firms, 1);
  for(std::set<repast::Agent*>::iterator firm_it = firms.begin();
      firm_it != firms.end(); firm_it++) {
    tmpFirm = dynamic_cast<Firm*> (*firm_it);
    tmpFirm->produceGoods(LabourProductivity);
  }
}

// consumer send order for goods
void BLModel::ConsumerConsumeGoods() {
  int myRank = repast::RepastProcess::instance()->rank();
  int worldSize = repast::RepastProcess::instance()->worldSize();

#ifdef TIMING
  double t1;
  int timer = 1;

  std::cout << myRank << ": Consumer consuming at "
            << repast::RepastProcess::instance()->getScheduleRunner().currentTick()
            << std::endl;
  std::cout << " -> 0. Context size on rank " << myRank
            << ": " << context.size() << "  ||"
            << "  GMN: " << GoodsMarketNetwork->edgeCount()
            << "  GDN: " << GoodsDeliveryNetwork->edgeCount()
            << " | LMN: " << LabourMarketNetwork->edgeCount()
            << "  AJN: " << AcceptedJobNetwork->edgeCount()
            << "  LJN: " << LeaveJobNetwork->edgeCount()
            << "  LSN: " << LabourSearchNetwork->edgeCount()
            << "  JON: " << JobOfferNetwork->edgeCount()
            << " | WPN: " << WagePaymentNetwork->edgeCount()
            << "  DPN: " << DividendPaymentNetwork->edgeCount()
            << std::endl;

  if(repast::RepastProcess::instance()->rank() == 0) {
    t1 = MPI_Wtime();
  }
#endif


  // get all local consumers
  std::vector<repast::Agent*> consumers;
  Cons* tmpCons;
  context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
                       consumers, 2);
  std::vector<repast::Agent*> adjEdges;
  std::vector<repast::Agent*>::iterator edge_it;
  for(std::vector<repast::Agent*>::iterator cons_it = consumers.begin();
      cons_it != consumers.end(); cons_it++) {
    tmpCons = dynamic_cast<Cons*> (*cons_it);

    // firm respond by delivering the requested amount
    // consumer consume the delivered amount
    tmpCons->sendOrder(maxNrFirms_buyGoods, GoodsMarketNetwork);
  }

#ifdef TIMING
  if(repast::RepastProcess::instance()->rank() == 0) {
    std::cout << timer++ << ": " << (MPI_Wtime() - t1) * 1000 << std::endl;
    t1 = MPI_Wtime();
  }
#endif

  GoodsMarketNetwork->balance();
  GoodsDeliveryNetwork->balance();


  // sync the projection
  repast::RepastProcess::instance()->synchronizeProjectionInfo<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);

#ifdef TIMING
  std::cout << " -> 1. Context size on rank " << myRank
            << ": " << context.size() << "  ||"
            << "  GMN: " << GoodsMarketNetwork->edgeCount()
            << "  GDN: " << GoodsDeliveryNetwork->edgeCount()
            << std::endl;
#endif

  // firms send their deliveries back
  std::vector<repast::Agent*> firms;
  Firm* tmpFirm;
  context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
                       firms, 1);
  adjEdges.clear();
  for(std::vector<repast::Agent*>::iterator firm_it = firms.begin();
      firm_it != firms.end(); firm_it++) {
    tmpFirm = dynamic_cast<Firm*> (*firm_it);

    // delete old links
    GoodsDeliveryNetwork->adjacent(tmpFirm, adjEdges);
    if(adjEdges.size() > 0) {
      for(edge_it = adjEdges.begin(); edge_it != adjEdges.end(); edge_it++) {
        GoodsDeliveryNetwork->removeEdge(tmpFirm, *edge_it);
      }
    }
    adjEdges.clear();
    tmpFirm->sendGoods(GoodsMarketNetwork, GoodsDeliveryNetwork);
  }


#ifdef TIMING
  if(repast::RepastProcess::instance()->rank() == 0) {
    std::cout << timer++ << ": " << (MPI_Wtime() - t1) * 1000 << std::endl;
    t1 = MPI_Wtime();
  }
#endif

  // sync the projection
  repast::RepastProcess::instance()->synchronizeProjectionInfo<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);

#ifdef TIMING
  std::cout << " -> 2. Context size on rank " << myRank
            << ": " << context.size() << "  ||"
            << "  GMN: " << GoodsMarketNetwork->edgeCount()
            << "  GDN: " << GoodsDeliveryNetwork->edgeCount()
            << std::endl;
#endif

  // consumers consume the goods
  consumers.clear();
  context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
                       consumers, 2);
  for(std::vector<repast::Agent*>::iterator cons_it = consumers.begin();
      cons_it != consumers.end(); cons_it++) {
    tmpCons = dynamic_cast<Cons*> (*cons_it);
    tmpCons->consumeGoods(GoodsDeliveryNetwork);
  }

#ifdef TIMING
  if(repast::RepastProcess::instance()->rank() == 0) {
    std::cout << timer++ << ": " << (MPI_Wtime() - t1) * 1000 << std::endl;
    t1 = MPI_Wtime();
  }
#endif

}
