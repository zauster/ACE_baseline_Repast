#include <stdio.h>
#include <vector>
#include <map>
#include <boost/mpi.hpp>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/initialize_random.h"
#include "repast_hpc/SVDataSetBuilder.h"

#include "BLModel.hpp"
#include "AgentPackage.hpp"

/* Serializable Agent Package Data */
AgentPackage::AgentPackage(){ }
// Firm variables
AgentPackage::AgentPackage(int _id, int _rank, int _type, int _currentRank,
                           double _Firm_Liquidity,
                           double _marginalCosts,
                           bool _openVacancy,
                           bool _FireAnEmployee,
                           int _OutstandingJobOffers,
                           bool _openVacancy_lastMonth,
                           int _MonthsSinceLastVacancy,
                           double _currentWage,
                           int _NrEmployees,
                           double _currentInventories,
                           double _currentDemand,
                           double _currentPrice,
                           std::vector<int> _EmployeeList):
  id(_id),
  rank(_rank),
  type(_type),
  currentRank(_currentRank),
  Firm_Liquidity(_Firm_Liquidity),
  marginalCosts(_marginalCosts),
  openVacancy(_openVacancy),
  FireAnEmployee(_FireAnEmployee),
  OutstandingJobOffers(_OutstandingJobOffers),
  openVacancy_lastMonth(_openVacancy_lastMonth),
  MonthsSinceLastVacancy(_MonthsSinceLastVacancy),
  currentWage(_currentWage),
  NrEmployees(_NrEmployees),
  currentInventories(_currentInventories),
  currentDemand(_currentDemand),
  currentPrice(_currentPrice),
  EmployeeList(_EmployeeList) {}

// Consumer variables
AgentPackage::AgentPackage(int _id, int _rank, int _type, int _currentRank,
                           double _Cons_Liquidity,
                           bool _isUnemployed,
                           int _EmployerID,
                           double _ReservationWage,
                           double _ReceivedWage,
                           double _ReceivedDividend,
                           double _plannedDemand,
                           double _dailyDemand,
                           double _realizedDemand,
                           double _realizedDemand_lastMonth,
                           double _unsatisfiedDemand,
                           // std::vector<int> _mySellerFirms,
                           std::map<int, double> _mySellerFirms_currPrices):
  id(_id),
  rank(_rank),
  type(_type),
  currentRank(_currentRank),
  Cons_Liquidity(_Cons_Liquidity),
  isUnemployed(_isUnemployed),
  EmployerID(_EmployerID),
  ReservationWage(_ReservationWage),
  ReceivedWage(_ReceivedWage),
  ReceivedDividend(_ReceivedDividend),
  plannedDemand(_plannedDemand),
  dailyDemand(_dailyDemand),
  realizedDemand(_realizedDemand),
  realizedDemand_lastMonth(_realizedDemand_lastMonth),
  unsatisfiedDemand(_unsatisfiedDemand),
  // mySellerFirms(_mySellerFirms),
  mySellerFirms_currPrices(_mySellerFirms_currPrices) {}

AgentPackageProvider::AgentPackageProvider(repast::SharedContext<repast::Agent>* agentPtr): agents(agentPtr){ }

void AgentPackageProvider::providePackage(repast::Agent* agent,
                                          std::vector<AgentPackage>& out){
  repast::AgentId id = agent->getId();
  if(id.agentType() == 1) { // it is a firm
    Firm* firm = dynamic_cast<Firm*>(agent);
    AgentPackage package(id.id(), id.startingRank(), id.agentType(),
                         id.currentRank(),
                         firm->getFirm_Liquidity(),
                         firm->getmarginalCosts(),
                         firm->getopenVacancy(),
                         firm->getFireAnEmployee(),
                         firm->getOutstandingJobOffers(),
                         firm->getopenVacancy_lastMonth(),
                         firm->getMonthsSinceLastVacancy(),
                         firm->getcurrentWage(),
                         firm->getNrEmployees(),
                         firm->getcurrentInventories(),
                         firm->getcurrentDemand(),
                         firm->getcurrentPrice(),
                         firm->getEmployeeList());
    out.push_back(package);
  } else if (id.agentType() == 2) { // a consumer
    Cons* cons = dynamic_cast<Cons*>(agent);
    AgentPackage package(id.id(), id.startingRank(), id.agentType(),
                         id.currentRank(),
                         cons->getCons_Liquidity(),
                         cons->getisUnemployed(),
                         cons->getEmployerID(),
                         cons->getReservationWage(),
                         cons->getReceivedWage(),
                         cons->getReceivedDividend(),
                         cons->getplannedDemand(),
                         cons->getdailyDemand(),
                         cons->getrealizedDemand(),
                         cons->getrealizedDemand_lastMonth(),
                         cons->getunsatisfiedDemand(),
                         // cons->getmySellerFirms(),
                         cons->getmySellerFirms_currPrices());
    out.push_back(package);
  }
}


void AgentPackageProvider::provideContent(repast::AgentRequest req, std::vector<AgentPackage>& out){
  std::vector<repast::AgentId> ids = req.requestedAgents();
  for(size_t i = 0; i < ids.size(); i++){
    providePackage(agents->getAgent(ids[i]), out);
  }
}


AgentPackageReceiver::AgentPackageReceiver(repast::SharedContext<repast::Agent>* agentPtr): agents(agentPtr){}

repast::Agent* AgentPackageReceiver::createAgent(AgentPackage package){
  repast::AgentId id(package.id, package.rank, package.type, package.currentRank);
  if(id.agentType() == 1) { // a firm
    return new Firm(id,
                    package.Firm_Liquidity,
                    package.marginalCosts,
                    package.openVacancy,
                    package.FireAnEmployee,
                    package.OutstandingJobOffers,
                    package.openVacancy_lastMonth,
                    package.MonthsSinceLastVacancy,
                    package.currentWage,
                    package.NrEmployees,
                    package.currentInventories,
                    package.currentDemand,
                    package.currentPrice,
                    package.EmployeeList);
  } else if(id.agentType() == 2) { // a consumer
    return new Cons(id,
                    package.isUnemployed,
                    package.EmployerID,
                    package.Cons_Liquidity,
                    package.ReservationWage,
                    package.ReceivedWage,
                    package.ReceivedDividend,
                    package.plannedDemand,
                    package.dailyDemand,
                    package.realizedDemand,
                    package.realizedDemand_lastMonth,
                    package.unsatisfiedDemand,
                    // package.mySellerFirms,
                    package.mySellerFirms_currPrices);
  }
}

void AgentPackageReceiver::updateAgent(AgentPackage package){
  repast::AgentId id(package.id, package.rank, package.type);
  if(id.agentType() == 1) { // a firm
    Firm* firm = dynamic_cast<Firm*>(agents->getAgent(id));
    firm->set(package.currentRank,
              package.Firm_Liquidity,
              package.marginalCosts,
              package.openVacancy,
              package.FireAnEmployee,
              package.OutstandingJobOffers,
              package.openVacancy_lastMonth,
              package.MonthsSinceLastVacancy,
              package.currentWage,
              package.NrEmployees,
              package.currentInventories,
              package.currentDemand,
              package.currentPrice,
              package.EmployeeList);
  } else if(id.agentType() == 2) { // a consumer
    Cons* cons = dynamic_cast<Cons*>(agents->getAgent(id));
    cons->set(package.currentRank,
              package.isUnemployed,
              package.EmployerID,
              package.Cons_Liquidity,
              package.ReservationWage,
              package.ReceivedWage,
              package.ReceivedDividend,
              package.plannedDemand,
              package.dailyDemand,
              package.realizedDemand,
              package.realizedDemand_lastMonth,
              package.unsatisfiedDemand,
              // package.mySellerFirms,
              package.mySellerFirms_currPrices);
  }
}
