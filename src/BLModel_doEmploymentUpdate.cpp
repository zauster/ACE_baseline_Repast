#include <stdio.h>
#include <vector>
#include <boost/mpi.hpp>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/initialize_random.h"

#include "BLModel.hpp"
#include "HelperFunctions.hpp"


void BLModel::doEmploymentUpdate() {
  int myRank = repast::RepastProcess::instance()->rank();
  int worldSize = repast::RepastProcess::instance()->worldSize();

  // std::cout << myRank << ": Doing employment status updates at "
  //           << repast::RepastProcess::instance()->getScheduleRunner().currentTick()
  //           << std::endl;

  // std::cout << " -> 0. Context size on rank " << myRank
  //           << ": " << context.size()
  //           << " LMN: " << LabourMarketNetwork->edgeCount()
  //           << " AJN: " << AcceptedJobNetwork->edgeCount()
  //           << " LJN: " << LeaveJobNetwork->edgeCount()
  //           << " LSN: " << LabourSearchNetwork->edgeCount()
  //           << " JON: " << JobOfferNetwork->edgeCount()
  //           << std::endl;

  // loop over firms and fire employees if necessary
  std::set<repast::Agent*> firms;
  Firm* tmpFirm;
  std::map<int, repast::AgentId> ListFiredWorkers;
  context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
                       firms, 1);
  for(std::set<repast::Agent*>::iterator firm_it = firms.begin();
      firm_it != firms.end(); firm_it++) {
    tmpFirm = dynamic_cast<Firm*> (*firm_it);
    tmpFirm->fireEmployee(LabourMarketNetwork);
  }

  // std::cout << " -> 1. Context size on rank " << myRank
  //           << ": " << context.size()
  //           << " LMN: " << LabourMarketNetwork->edgeCount()
  //           << " AJN: " << AcceptedJobNetwork->edgeCount()
  //           << " LJN: " << LeaveJobNetwork->edgeCount()
  //           << " LSN: " << LabourSearchNetwork->edgeCount()
  //           << " JON: " << JobOfferNetwork->edgeCount()
  //           << std::endl;

  // synchronization step needed. After sync consumer can ask if they
  // are still employed
  repast::RepastProcess::instance()->synchronizeProjectionInfo<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);

  // std::cout << " -> 2. Context size on rank " << myRank
  //           << ": " << context.size()
  //           << " LMN: " << LabourMarketNetwork->edgeCount()
  //           << " AJN: " << AcceptedJobNetwork->edgeCount()
  //           << " LJN: " << LeaveJobNetwork->edgeCount()
  //           << " LSN: " << LabourSearchNetwork->edgeCount()
  //           << " JON: " << JobOfferNetwork->edgeCount()
  //           << std::endl;

  // loop over consumers and make them check if they are still employed
  std::vector<repast::Agent*> allFirms;
  context.selectAgents(allFirms, 1);
  std::set<repast::Agent*> consumers;
  Cons* tmpCons;
  context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
                       consumers, 2);
  for(std::set<repast::Agent*>::iterator cons_it = consumers.begin();
      cons_it != consumers.end(); cons_it++) {
    tmpCons = dynamic_cast<Cons*> (*cons_it);

    tmpCons->checkEmployment(AcceptedJobNetwork, LabourMarketNetwork);

    tmpCons->findNewWork(LabourSearchNetwork, allFirms,
                         NrFirms, worldSize, maxNrFirms_openVacancy);
  }

  // Since the LabourSearchNetwork has been updated, do a sync-step
  repast::RepastProcess::instance()->synchronizeProjectionInfo<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);

  // std::cout << " -> 3. Context size on rank " << myRank
  //           << ": " << context.size()
  //           << " LMN: " << LabourMarketNetwork->edgeCount()
  //           << " AJN: " << AcceptedJobNetwork->edgeCount()
  //           << " LJN: " << LeaveJobNetwork->edgeCount()
  //           << " LSN: " << LabourSearchNetwork->edgeCount()
  //           << " JON: " << JobOfferNetwork->edgeCount()
  //           << std::endl;

  firms.clear();
  context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
                       firms, 1);
  for(std::set<repast::Agent*>::iterator firm_it = firms.begin();
      firm_it != firms.end(); firm_it++) {
    tmpFirm = dynamic_cast<Firm*> (*firm_it);
    tmpFirm->sendJobOffers(LabourSearchNetwork, JobOfferNetwork);
  }

  // Since the LabourSearchNetwork has been updated, do a sync-step
  repast::RepastProcess::instance()->synchronizeProjectionInfo<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);

  // std::cout << " -> 4. Context size on rank " << myRank
  //           << ": " << context.size()
  //           << " LMN: " << LabourMarketNetwork->edgeCount()
  //           << " AJN: " << AcceptedJobNetwork->edgeCount()
  //           << " LJN: " << LeaveJobNetwork->edgeCount()
  //           << " LSN: " << LabourSearchNetwork->edgeCount()
  //           << " JON: " << JobOfferNetwork->edgeCount()
  //           << std::endl;

  std::vector<repast::Agent*> adjEdges;
  std::vector<repast::Agent*>::iterator edge_it;

  // employers check if they have received job applications
  for(std::set<repast::Agent*>::iterator cons_it = consumers.begin();
      cons_it != consumers.end(); cons_it++) {
    tmpCons = dynamic_cast<Cons*> (*cons_it);

    tmpCons->checkJobOffers(JobOfferNetwork, LeaveJobNetwork,
                            AcceptedJobNetwork);

    // Remove any remaining edges that may still exist in the
    // LabourSearchNetwork. They are not needed anymore
    LabourSearchNetwork->adjacent(tmpCons, adjEdges);
    if(adjEdges.size() > 0) {
      for(edge_it = adjEdges.begin(); edge_it != adjEdges.end(); edge_it++) {
        LabourSearchNetwork->removeEdge(tmpCons, (*edge_it));
      }
    }
    adjEdges.clear();
  }

  repast::RepastProcess::instance()->synchronizeProjectionInfo<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);

  // std::cout << " -> 5. Context size on rank " << myRank
  //           << ": " << context.size()
  //           << " LMN: " << LabourMarketNetwork->edgeCount()
  //           << " AJN: " << AcceptedJobNetwork->edgeCount()
  //           << " LJN: " << LeaveJobNetwork->edgeCount()
  //           << " LSN: " << LabourSearchNetwork->edgeCount()
  //           << " JON: " << JobOfferNetwork->edgeCount()
  //           << std::endl;

  // firms check if their job offer has been accepted or if one of
  // their employees handed in their notice
  for(std::set<repast::Agent*>::iterator firm_it = firms.begin();
      firm_it != firms.end(); firm_it++) {
    tmpFirm = dynamic_cast<Firm*> (*firm_it);
    tmpFirm->checkAcceptedJobOffers(AcceptedJobNetwork, LeaveJobNetwork,
                                    LabourMarketNetwork);

    // Remove remaining edges on the JobOfferNetwork, not needed
    // anymore.
    JobOfferNetwork->adjacent(tmpFirm, adjEdges);
    if(adjEdges.size() > 0) {
      for(edge_it = adjEdges.begin(); edge_it != adjEdges.end(); edge_it++) {
        JobOfferNetwork->removeEdge(tmpFirm, (*edge_it));
      }
    }
    adjEdges.clear();
  }

  for(std::set<repast::Agent*>::iterator cons_it = consumers.begin();
      cons_it != consumers.end(); cons_it++) {
    tmpCons = dynamic_cast<Cons*> (*cons_it);
    // Remove any edges on the LeaveJobNetwork that may still exist
    // as they are not needed anymore.
    LeaveJobNetwork->adjacent(tmpCons, adjEdges);
    if(adjEdges.size() > 0) {
      for(edge_it = adjEdges.begin(); edge_it != adjEdges.end(); edge_it++) {
        LeaveJobNetwork->removeEdge(tmpCons, (*edge_it));
      }
    }
    adjEdges.clear();
  }

  repast::RepastProcess::instance()->synchronizeProjectionInfo<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);

  // std::cout << " -> 6. Context size on rank " << myRank
  //           << ": " << context.size()
  //           << " LMN: " << LabourMarketNetwork->edgeCount()
  //           << " AJN: " << AcceptedJobNetwork->edgeCount()
  //           << " LJN: " << LeaveJobNetwork->edgeCount()
  //           << " LSN: " << LabourSearchNetwork->edgeCount()
  //           << " JON: " << JobOfferNetwork->edgeCount()
  //           << std::endl;

}

// firms update their wages
// firms update their prices
void BLModel::doFirmsPricesUpdate() {
  int myRank = repast::RepastProcess::instance()->rank();
  int worldSize = repast::RepastProcess::instance()->worldSize();

  // std::cout << myRank << ": Firms updating prices and wages at "
  //           << repast::RepastProcess::instance()->getScheduleRunner().currentTick()
  //           << std::endl;

  std::set<repast::Agent*> firms;
  Firm* tmpFirm;
  context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
                       firms, 1);
  for(std::set<repast::Agent*>::iterator firm_it = firms.begin();
      firm_it != firms.end(); firm_it++) {
    tmpFirm = dynamic_cast<Firm*> (*firm_it);
    tmpFirm->updatePrices(lowerBoundInventories,
                          upperBoundInventories,
                          lowerBoundPrice,
                          upperBoundPrice,
                          theta, ProbNewPrice);
    tmpFirm->updateWages(gamma, delta);
  }
}
