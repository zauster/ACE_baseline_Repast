#include <stdio.h>
#include <vector>
#include <boost/mpi.hpp>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/initialize_random.h"

#include "BLModel.hpp"
#include "HelperFunctions.hpp"


// firms pay wages
// firms pay dividends
void BLModel::doWagePayments() {
  int myRank = repast::RepastProcess::instance()->rank();
  int worldSize = repast::RepastProcess::instance()->worldSize();

  repast::RepastProcess::instance()->synchronizeProjectionInfo<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);

  // std::cout << myRank << ": Doing wage payment at "
  //           << repast::RepastProcess::instance()->getScheduleRunner().currentTick()
  //           << std::endl;
  // std::cout << " -> 0. Context size on rank " << myRank
  //           << ": " << context.size()
  //           << " WPN: " << WagePaymentNetwork->edgeCount()
  //           << " DPN: " << DividendPaymentNetwork->edgeCount()
  //           << std::endl;

  repast::AgentRequest ConsumerRequest(myRank);
  for(int id = 1; id <= NrConsumers; id++) {
    if(id % worldSize != myRank) {
      repast::AgentId tmpID(id, id % worldSize, 2);
      ConsumerRequest.addRequest(tmpID);
    }
  }

  repast::RepastProcess::instance()->requestAgents<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, ConsumerRequest, *agent_provider, *agent_receiver, *agent_receiver);


  std::vector<repast::Agent*> firms;
  std::vector<repast::Agent*> allConsumers;
  std::vector<repast::Agent*> adjEdges;
  std::vector<repast::Agent*>::iterator edge_it;
  Firm* tmpFirm;
  context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
                       firms, 1);
  context.selectAgents(allConsumers, 2);
  for(std::vector<repast::Agent*>::iterator firm_it = firms.begin();
      firm_it != firms.end(); firm_it++) {
    tmpFirm = dynamic_cast<Firm*> (*firm_it);

    // pay the wages
    // WagePaymentNetwork->adjacent(tmpFirm, adjEdges);
    // for(edge_it = adjEdges.begin(); edge_it != adjEdges.end(); edge_it++) {
    //   WagePaymentNetwork->removeEdge(tmpFirm, *edge_it);
    // }
    tmpFirm->payWages(WagePaymentNetwork, LabourMarketNetwork);

    // pay the dividends
    // adjEdges.clear();
    // DividendPaymentNetwork->adjacent(tmpFirm, adjEdges);
    // for(edge_it = adjEdges.begin(); edge_it != adjEdges.end(); edge_it++) {
    //   DividendPaymentNetwork->removeEdge(tmpFirm, *edge_it);
    // }
    tmpFirm->payDividend(DividendPaymentNetwork, allConsumers);
  }

  repast::RepastProcess::instance()->synchronizeProjectionInfo<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);

  // std::cout << " -> 1. Context size on rank " << myRank
  //           << ": " << context.size()
  //           << " WPN: " << WagePaymentNetwork->edgeCount()
  //           << " DPN: " << DividendPaymentNetwork->edgeCount()
  //           << std::endl;

  // consumer adjust their reservation wage get all local consumers
  std::vector<repast::Agent*> consumers;
  Cons* tmpCons;
  context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
                       consumers, 2);
  for(std::vector<repast::Agent*>::iterator cons_it = consumers.begin();
      cons_it != consumers.end(); cons_it++) {
    tmpCons = dynamic_cast<Cons*> (*cons_it);
    // Consumer receive and book the wage and possible dividend
    // payments
    tmpCons->recordWagePayments(WagePaymentNetwork, DividendPaymentNetwork,
                                AcceptedJobNetwork);
    // let the consumer adjust their reservation wage
    tmpCons->adjReservationWage();
  }

  // clear the two networks of all wages
  for(std::vector<repast::Agent*>::iterator firm_it = firms.begin();
      firm_it != firms.end(); firm_it++) {
    tmpFirm = dynamic_cast<Firm*> (*firm_it);

    // Wage Network
    WagePaymentNetwork->adjacent(tmpFirm, adjEdges);
    for(edge_it = adjEdges.begin(); edge_it != adjEdges.end(); edge_it++) {
      WagePaymentNetwork->removeEdge(tmpFirm, *edge_it);
    }

    // Dividend Network
    adjEdges.clear();
    DividendPaymentNetwork->adjacent(tmpFirm, adjEdges);
    for(edge_it = adjEdges.begin(); edge_it != adjEdges.end(); edge_it++) {
      DividendPaymentNetwork->removeEdge(tmpFirm, *edge_it);
    }
  }

  repast::RepastProcess::instance()->synchronizeProjectionInfo<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);

  // std::cout << " -> 2. Context size on rank " << myRank
  //           << ": " << context.size()
  //           << " WPN: " << WagePaymentNetwork->edgeCount()
  //           << " DPN: " << DividendPaymentNetwork->edgeCount()
  //           << std::endl;

}

void BLModel::writeAgentStates() {
  int myRank = repast::RepastProcess::instance()->rank();
  int worldSize = repast::RepastProcess::instance()->worldSize();
  double tick = repast::RepastProcess::instance()->getScheduleRunner().currentTick();
  int inttick = (int) tick;
  std::cout << myRank << ": Writing agent states at " << tick << std::endl;

  std::ofstream logfile;
  std::string pathstub = "output/log_";
  std::string consstub = "_cons.csv";
  std::string filename = pathstub + std::to_string(myRank) + "_" + std::to_string(inttick) + consstub;
  logfile.open(filename);
  logfile << "Cons_ID,isUnemployed,EmployerID,Cons_Liquidity,"
          << "ReservationWage,ReceivedWage,ReceivedDividend,"
          << "plannedDemand,dailyDemand,realizedDemand,"
          << "realizedDemand_lastMonth,unsatisfiedDemand,"
          << "mySellerFirms"
          << std::endl;

  std::vector<repast::Agent*> consumers;
  Cons* tmpCons;
  context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
                       consumers, 2);
  for(std::vector<repast::Agent*>::iterator cons_it = consumers.begin();
      cons_it != consumers.end(); cons_it++) {
    tmpCons = dynamic_cast<Cons*> (*cons_it);
    tmpCons->writeOut(logfile);
  }
  logfile.close();

  filename.clear();
  std::string firmstub = "_firm.csv";
  filename = pathstub + std::to_string(myRank) + "_" + std::to_string(inttick) + firmstub;
  logfile.open(filename);
  logfile << "Firm_ID,Firm_Liquidity,marginalCosts,openVacancy,FireAnEmployee,OutstandingJobOffers,openVacancy_lastMonth,MonthsSinceLastVacancy,currentWage,NrEmployees,currentInventories,currentDemand,currentPrice,EmployeeList" << std::endl;

  std::vector<repast::Agent*> firms;
  Firm* tmpFirm;
  context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
                       firms, 1);
  for(std::vector<repast::Agent*>::iterator firm_it = firms.begin();
      firm_it != firms.end(); firm_it++) {
    tmpFirm = dynamic_cast<Firm*> (*firm_it);
    tmpFirm->writeOut(logfile);
  }
  logfile.close();

}
