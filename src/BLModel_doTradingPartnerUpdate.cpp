#include <stdio.h>
#include <vector>
#include <boost/mpi.hpp>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/initialize_random.h"

#include "BLModel.hpp"
#include "HelperFunctions.hpp"


// consumer look for new trading partner
void BLModel::updateTradingPartners() {
  int myRank = repast::RepastProcess::instance()->rank();
  int worldSize = repast::RepastProcess::instance()->worldSize();

  // std::cout << myRank << ": Consumer updating trading partner at "
  //           << repast::RepastProcess::instance()->getScheduleRunner().currentTick()
  //           << std::endl;

  // std::cout << " -> Context size on rank " << myRank
  //           << ": " << context.size()
  //           << " GMN: " << GoodsMarketNetwork->edgeCount()
  //           << " GDN: " << GoodsDeliveryNetwork->edgeCount()
  //           << std::endl;

  // get all local consumers
  std::vector<repast::Agent*> consumers;
  std::vector<repast::Agent*> firms;
  Cons* tmpCons;
  context.selectAgents(firms, 1);
  context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
                       consumers, 2);
  for(std::vector<repast::Agent*>::iterator cons_it = consumers.begin();
      cons_it != consumers.end(); cons_it++) {
    tmpCons = dynamic_cast<Cons*> (*cons_it);
    tmpCons->updateTradingPartners(GoodsMarketNetwork, NrFirms_PriceSwitch,
                                   worldSize, firms);
  }
  repast::RepastProcess::instance()->synchronizeProjectionInfo<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);

}


// consumer ask for current prices and consumer plan their demand for
// the coming month
void BLModel::doConsumerPricesUpdate() {
  int myRank = repast::RepastProcess::instance()->rank();
  int worldSize = repast::RepastProcess::instance()->worldSize();

  // std::cout << myRank << ": Consumer updating prices at "
  //           << repast::RepastProcess::instance()->getScheduleRunner().currentTick()
  //           << std::endl;
  // std::cout << " -> Context size on rank " << myRank
  //           << ": " << context.size()
  //           << " GMN: " << GoodsMarketNetwork->edgeCount()
  //           << " GDN: " << GoodsDeliveryNetwork->edgeCount()
  //           << std::endl;

  // get all local consumers
  std::vector<repast::Agent*> consumers;
  Cons* tmpCons;
  context.selectAgents(repast::SharedContext<repast::Agent>::LOCAL,
                       consumers, 2);
  for(std::vector<repast::Agent*>::iterator cons_it = consumers.begin();
      cons_it != consumers.end(); cons_it++) {
    tmpCons = dynamic_cast<Cons*> (*cons_it);
    tmpCons->updatePrices(GoodsMarketNetwork, epsilon);
  }



  GoodsMarketNetwork->balance();
  GoodsDeliveryNetwork->balance();

  repast::RepastProcess::instance()->synchronizeProjectionInfo<repast::Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(context, *agent_provider, *agent_receiver, *agent_receiver);

}

