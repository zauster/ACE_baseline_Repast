#include "Cons.hpp"

Cons::Cons(repast::AgentId id): Cons_ID(id), Cons_Liquidity(10),
                                plannedDemand(20){ }
Cons::Cons(repast::AgentId _Cons_ID,
           bool _isUnemployed,
           int _EmployerID,
           double _Cons_Liquidity,
           double _ReservationWage,
           double _ReceivedWage,
           double _ReceivedDividend,
           double _plannedDemand,
           double _dailyDemand,
           double _realizedDemand,
           double _realizedDemand_lastMonth,
           double _unsatisfiedDemand,
           // std::vector<int> _mySellerFirms,
           std::map<int, double> _mySellerFirms_currPrices):
  Cons_ID(_Cons_ID),
  isUnemployed(_isUnemployed),
  EmployerID(_EmployerID),
  Cons_Liquidity(_Cons_Liquidity),
  ReservationWage(_ReservationWage),
  ReceivedWage(_ReceivedWage),
  ReceivedDividend(_ReceivedDividend),
  plannedDemand(_plannedDemand),
  dailyDemand(_dailyDemand),
  realizedDemand(_realizedDemand),
  realizedDemand_lastMonth(_realizedDemand_lastMonth),
  unsatisfiedDemand(_unsatisfiedDemand),
  // mySellerFirms(_mySellerFirms),
  mySellerFirms_currPrices(_mySellerFirms_currPrices) {}

Cons::~Cons(){ }

void Cons::set(int currentRank,
               bool _isUnemployed,
               int _EmployerID,
               double _Cons_Liquidity,
               double _ReservationWage,
               double _ReceivedWage,
               double _ReceivedDividend,
               double _plannedDemand,
               double _dailyDemand,
               double _realizedDemand,
               double _realizedDemand_lastMonth,
               double _unsatisfiedDemand,
               // std::vector<int> _mySellerFirms,
               std::map<int, double> _mySellerFirms_currPrices) {
  Cons_ID.currentRank(currentRank);
  isUnemployed = _isUnemployed;
  EmployerID = _EmployerID;
  Cons_Liquidity = _Cons_Liquidity;
  ReservationWage = _ReservationWage;
  ReceivedWage = _ReceivedWage;
  ReceivedDividend = _ReceivedDividend;
  plannedDemand = _plannedDemand;
  dailyDemand = _dailyDemand;
  realizedDemand = _realizedDemand;
  realizedDemand_lastMonth = _realizedDemand_lastMonth;
  unsatisfiedDemand = _unsatisfiedDemand;
  // mySellerFirms = _mySellerFirms;
  mySellerFirms_currPrices = _mySellerFirms_currPrices;
}


void Cons::checkEmployment(repast::SharedNetwork<repast::Agent,
                           repast::RepastEdge<repast::Agent>,
                           repast::RepastEdgeContent<repast::Agent>,
                           repast::RepastEdgeContentManager<repast::Agent> >* AcceptedJobNetwork, repast::SharedNetwork<repast::Agent,
                           repast::RepastEdge<repast::Agent>,
                           repast::RepastEdgeContent<repast::Agent>,
                           repast::RepastEdgeContentManager<repast::Agent> >* LabourMarketNetwork) {
  // get employer, check if still employed
  std::vector<repast::Agent*> Employer;
  std::vector<repast::Agent*> MyEmployer;
  LabourMarketNetwork->adjacent(this, Employer);
  if(Employer.size() == 1) {

    // check if the two employers are the same: the edge in the
    // LabourMarketNetwork is set by the employer, the edge in the
    // AcceptedJobNetwork by the consumer. That's why they must at all
    // times coincide.
    AcceptedJobNetwork->adjacent(this, MyEmployer);
    if(MyEmployer.size() == 1) {
      if(MyEmployer[0]->getId() == Employer[0]->getId()) {
        // everything correct
      } else {
        throw std::runtime_error( "Differing employers!");
      }
    } else {
      throw std::runtime_error( "Differing number of employers!");
      assert(false);
    }
    isUnemployed = false;

  } else if(Employer.size() == 0) {
    isUnemployed = true;

    // remove edge in the Accepted Job Network, if there is one (if
    // yes, we have just been fired!)
    AcceptedJobNetwork->adjacent(this, MyEmployer);
    if(MyEmployer.size() > 0) {
      AcceptedJobNetwork->removeEdge(this, MyEmployer[0]);

#ifdef EMPVERBOSE
      std::cout << "Cons " << this->getId().id() << " has just been fired!"
                << std::endl;
#endif

    }
  } else {
    assert(false);
  }
}

void Cons::findNewWork(repast::SharedNetwork<repast::Agent,
                       repast::RepastEdge<repast::Agent>,
                       repast::RepastEdgeContent<repast::Agent>,
                       repast::RepastEdgeContentManager<repast::Agent> >* LabourSearchNetwork,
                       std::vector<repast::Agent*> firms,
                       int NrFirms, int NrProc,
                       int maxNrFirms_openVacancy) {
  // will contain the firms we already sent job offers
  std::set<repast::AgentId> pickedFirms;

  // if the consumer is unemployed, he will send the most job offers
  // if he is underpaid, he will send one
  // if he is employed and well payed, he will look with prop p
  if(isUnemployed) {
    // maxNrFirms_openVacancy is left as it is: default = 4
  } else if(ReceivedWage < ReservationWage) {
    // maxNrFirms is set to one
    maxNrFirms_openVacancy = 1;
    // we don't want to apply at the current employer
    repast::AgentId tmpID(EmployerID, EmployerID % NrProc, 1);
    pickedFirms.insert(tmpID);
  } else {
    // maxNrFirms is set to one with a prob of 0.1
    maxNrFirms_openVacancy = 0;
    if(getRandDouble(0.0, 1.0) < 0.1) {
      maxNrFirms_openVacancy = 1;
      repast::AgentId tmpID(EmployerID, EmployerID % NrProc, 1);
      pickedFirms.insert(tmpID);
    }
  }

  for(int i = 0; i < maxNrFirms_openVacancy; i++) {
    // pick a random firm
    int ri = getRandIndex(NrFirms);
    Firm* tmpFirm = dynamic_cast<Firm *>(firms[ri]);
    // as long as I already picked this firm, try a new firm
    while(pickedFirms.find(tmpFirm->getId()) != pickedFirms.end()) {
      ri = getRandIndex(NrFirms);
      tmpFirm = dynamic_cast<Firm *>(firms[ri]);
    }
    pickedFirms.insert(tmpFirm->getId());
    LabourSearchNetwork->addEdge(this, tmpFirm, ReservationWage);

#ifdef EMPVERBOSE
    std::cout << "Cons " << this->getId()
              << " applies at firm "
              << tmpFirm->getId()
              << std::endl;
#endif

  }
}

void Cons::checkJobOffers(repast::SharedNetwork<repast::Agent,
                          repast::RepastEdge<repast::Agent>,
                          repast::RepastEdgeContent<repast::Agent>,
                          repast::RepastEdgeContentManager<repast::Agent> >* JobOfferNetwork, repast::SharedNetwork<repast::Agent,
                          repast::RepastEdge<repast::Agent>,
                          repast::RepastEdgeContent<repast::Agent>,
                          repast::RepastEdgeContentManager<repast::Agent> >* LeaveJobNetwork, repast::SharedNetwork<repast::Agent,
                          repast::RepastEdge<repast::Agent>,
                          repast::RepastEdgeContent<repast::Agent>,
                          repast::RepastEdgeContentManager<repast::Agent> >* AcceptedJobNetwork) {
  std::vector<repast::Agent*> JobOffers;
  JobOfferNetwork->adjacent(this, JobOffers);
  bool hasAcceptedJobOffer = false;

  if(JobOffers.size() > 0) {
#ifdef EMPVERBOSE
    std::cout << "Cons " << this->getId().id() << " rec "
              << JobOffers.size() << " JO. ";
#endif

    // get random index from the vector of job offers
    int ri = getRandIndex(JobOffers.size());

#ifdef EMPVERBOSE
    std::cout << "Cons " << this->getId().id() << " acc JO from firm "
              << JobOffers[ri]->getId().id() << std::endl;
#endif

    // in case the consumer is employed at the moment, have to
    // remove the edge to this former employer
    std::vector<repast::Agent*> Employer;
   if(not isUnemployed) {
      AcceptedJobNetwork->adjacent(this, Employer);
      if(Employer.size() > 1) {
        throw std::runtime_error( "Too many employers here!");
      }
      if(Employer.size() > 0) {

#ifdef EMPVERBOSE
        std::cout << "Cons " << this->getId().id() << " leaving firm "
                  << Employer[0]->getId().id() << " in fav of firm "
                  << JobOffers[ri]->getId().id() << std::endl;
#endif

        AcceptedJobNetwork->removeEdge(this, Employer[0]);
        LeaveJobNetwork->addEdge(this, Employer[0]);
      }
    }

    // and set the 'new' employer
    EmployerID = JobOffers[ri]->getId().id();
    isUnemployed = false;
    hasAcceptedJobOffer = true;
    AcceptedJobNetwork->addEdge(this, JobOffers[ri]);
  }
}

void Cons::updateTradingPartners(repast::SharedNetwork<repast::Agent,
                                 repast::RepastEdge<repast::Agent>,
                                 repast::RepastEdgeContent<repast::Agent>,
                                 repast::RepastEdgeContentManager<repast::Agent> >* GoodsMarketNetwork,
                                 int NrFirms_PriceSwitch,
                                 int NrProc,
                                 std::vector<repast::Agent*> firms) {
  double maxPrice;
  int maxPriceFirm, ri;
  std::vector<repast::Agent*> currentPartners;
  std::vector<repast::Agent*>::iterator it;
  GoodsMarketNetwork->adjacent(this, currentPartners);
  Firm* tmpFirm;

  // std::cout << "Cons " << this->getId();
  for(int i = 0; i < NrFirms_PriceSwitch; i++) {
    maxPrice = 0.0;
    maxPriceFirm = 0;
    // find firm with highest price in the set of trading partners
    // std::cout << " (";
    for(std::map<int, double>::iterator it = mySellerFirms_currPrices.begin();
        it != mySellerFirms_currPrices.end(); it++) {
      if(it->second > maxPrice) {
        maxPrice = it->second;
        maxPriceFirm = it->first;
        // std::cout << maxPriceFirm << ": " << maxPrice;
      }
    }
    // create the ID for the firm with the maximal price
    repast::AgentId tmpID(maxPriceFirm, maxPriceFirm % NrProc, 1);

    // find the corresponding agent and remove that edge
    for(it = currentPartners.begin(); it != currentPartners.end(); i++) {
      if((*it)->getId() == tmpID) {
        tmpFirm = dynamic_cast<Firm*>((*it));
        break;
      }
    }
    GoodsMarketNetwork->removeEdge(this, tmpFirm);

    // add a new edge to a random firm, given it is not already one of
    // our trading partners
    do {
      ri = getRandIndex(firms.size());
    } while(mySellerFirms_currPrices.find(firms[ri]->getId().id()) != mySellerFirms_currPrices.end());
    tmpFirm = dynamic_cast<Firm*>(firms[ri]);
    GoodsMarketNetwork->addEdge(this, tmpFirm);
    // std::cout << ". " << tmpFirm->getId().id() << ") " << std::endl;

    // insert the new trading partner into the map
    std::pair<int, double> tmpKey(tmpFirm->getId().id(), 0.0);
    mySellerFirms_currPrices.insert(tmpKey);

    // now we can also remove the evicted trading partner from our set
    // of trading partners
    mySellerFirms_currPrices.erase(maxPriceFirm);
  }

}

void Cons::updatePrices(repast::SharedNetwork<repast::Agent,
                        repast::RepastEdge<repast::Agent>,
                        repast::RepastEdgeContent<repast::Agent>,
                        repast::RepastEdgeContentManager<repast::Agent> >* GoodsMarketNetwork,
                        double epsilon) {
  std::vector<repast::Agent*> SellerFirms;
  GoodsMarketNetwork->adjacent(this, SellerFirms);
  double tmpPrice;
  mySellerFirms_currPrices.clear();
  // std::cout << "Cons " << this->getId().id() << " => ";
  for(int i = 0; i < SellerFirms.size(); i++) {
    Firm* tmpFirm = dynamic_cast<Firm*>(SellerFirms[i]);
    tmpPrice = tmpFirm->getcurrentPrice();

    std::pair<int, double> tmpPriceKey(tmpFirm->getId().id(),
                                       tmpPrice);
    // std::cout << " (" << tmpPriceKey.first << ": "
    // << tmpPriceKey.second << ")";
    mySellerFirms_currPrices.insert(tmpPriceKey);
  }
  // std::cout << std::endl;

  // now that the consumer has updated the firms' prices
  // he'll also update his planned demand

  // calc the average price
  double avgPrice = 0;
  for(std::map<int, double>::iterator it = mySellerFirms_currPrices.begin();
      it != mySellerFirms_currPrices.end(); it++) {
    avgPrice += it->second / mySellerFirms_currPrices.size();
  }

  if(Cons_Liquidity > epsilon) {
    plannedDemand = pow(Cons_Liquidity / avgPrice, 0.9);
  } else {
    plannedDemand = 0;
  }
  unsatisfiedDemand = 0;
  realizedDemand = 0;

}

void Cons::sendOrder(int maxNrFirms_buyGoods,
                     repast::SharedNetwork<repast::Agent,
                     repast::RepastEdge<repast::Agent>,
                     repast::RepastEdgeContent<repast::Agent>,
                     repast::RepastEdgeContentManager<repast::Agent> >* GoodsMarketNetwork) {
  // plan daily demand
  dailyDemand = (plannedDemand / 20);
  dailyDemand = dailyDemand / maxNrFirms_buyGoods;
  std::vector<repast::Agent*> SellerFirms;
  std::vector<repast::Agent*>::iterator it;
  GoodsMarketNetwork->adjacent(this, SellerFirms);

  // before ordering, re-set all edges to 0, invalidating all previous
  // orders
  for(it = SellerFirms.begin(); it != SellerFirms.end(); it++) {
    GoodsMarketNetwork->addEdge(this, *it, 0.0);
  }

  // std::cout << "Cons " << this->getId().id() << ":  ";
  // for(it = SellerFirms.begin(); it != SellerFirms.end(); it++) {
  //   std::cout << " (" << (*it)->getId().id() << ": "
  //             << GoodsMarketNetwork->findEdge(this, *it)->weight() << ") ";
  // }
  // std::cout << std::endl;

  if(Cons_Liquidity > 0) {
    // dailyBudget is just to keep track of the spending
    double dailyBudget = Cons_Liquidity;
    double requestedAmount, deliveredAmount;
    int ri;
    Firm* tmpFirm;

    std::set<repast::AgentId> FirmsOrderedFrom;
    for(int i = 0; i < maxNrFirms_buyGoods; i++) {
      do {
        ri = getRandIndex(SellerFirms.size());
        tmpFirm = dynamic_cast<Firm*>(SellerFirms[ri]);
        // check if firm has been chosen, if yes, draw another random number
      } while(FirmsOrderedFrom.find(tmpFirm->getId()) != FirmsOrderedFrom.end());
      FirmsOrderedFrom.insert(tmpFirm->getId());
      double tmpPrice = this->getmySellerFirms_currPrices()[tmpFirm->getId().id()];

      if(tmpPrice * dailyDemand <= dailyBudget and dailyBudget > 0) {
        requestedAmount = dailyDemand;
      } else if(tmpPrice * dailyDemand > dailyBudget and dailyBudget > 0) {
        requestedAmount = dailyBudget / tmpPrice;
      } else if(dailyBudget <= 0) {
        requestedAmount = 0;
        break;
      }

      if(requestedAmount < 0) {
        std::cout << "XXXXX Cons " << this->getId().id()
                  << " @ " << tmpFirm->getId().id()
                  << "  price: " << tmpPrice
                  << "  bud: " << Cons_Liquidity
                  << "  db: " << dailyBudget
                  << "  req: " << requestedAmount
                  << "  dail: " << dailyDemand
                  << "  unsat: " << unsatisfiedDemand
                  << std::endl;
        requestedAmount = 0;
      }

      if(requestedAmount > 0.01) {
      // let's order that amount and see what the firm can deliver
      GoodsMarketNetwork->addEdge(this, tmpFirm, requestedAmount);
        dailyBudget -= requestedAmount * tmpPrice;
      }
    }
  }

  // std::cout << "Cons " << this->getId().id() << ":  ";
  // for(it = SellerFirms.begin(); it != SellerFirms.end(); it++) {
  //   std::cout << " (" << (*it)->getId().id() << ": "
  //             << GoodsMarketNetwork->findEdge(this, *it)->weight() << ") ";
  // }
  // std::cout << std::endl;

}

void Cons::consumeGoods(repast::SharedNetwork<repast::Agent,
                        repast::RepastEdge<repast::Agent>,
                        repast::RepastEdgeContent<repast::Agent>,
                        repast::RepastEdgeContentManager<repast::Agent> >* GoodsDeliveryNetwork) {
  std::vector<repast::Agent*> GoodsDeliveries;
  GoodsDeliveryNetwork->adjacent(this, GoodsDeliveries);
  double tmpPrice, deliveredAmount;
  unsatisfiedDemand = 0;

  for(std::vector<repast::Agent*>::iterator it = GoodsDeliveries.begin();
      it != GoodsDeliveries.end(); it++) {
    deliveredAmount = GoodsDeliveryNetwork->findEdge(this, *it)->weight();
    // std::cout << "Cons " << this->getId().id()
    //           << " del: " << deliveredAmount
    //           << std::endl;

    tmpPrice = this->getmySellerFirms_currPrices()[(*it)->getId().id()];

    // pay for the delivered amount
    Cons_Liquidity -= tmpPrice * deliveredAmount;

    // and consume the delivered amount
    realizedDemand += deliveredAmount;

    // the difference between planned and delivered amount will be
    // collected and tried to satified tomorrow. dailyDemand is that
    // amount that I wanted to consume yesterday
      unsatisfiedDemand += (dailyDemand - deliveredAmount);

    if(Cons_Liquidity < -0.1) {
      std::cout << "Cons " << this->getId().id()
                << " @ " << (*it)->getId().id()
                << "  price: " << tmpPrice
                << "  bud: " << Cons_Liquidity
                << "  del: " << deliveredAmount
                << std::endl;
    }
  }
}

void Cons::recordWagePayments(repast::SharedNetwork<repast::Agent,
                              repast::RepastEdge<repast::Agent>,
                              repast::RepastEdgeContent<repast::Agent>,
                              repast::RepastEdgeContentManager<repast::Agent> >* WagePaymentNetwork,
                              repast::SharedNetwork<repast::Agent,
                              repast::RepastEdge<repast::Agent>,
                              repast::RepastEdgeContent<repast::Agent>,
                              repast::RepastEdgeContentManager<repast::Agent> >* DividendPaymentNetwork,
                              repast::SharedNetwork<repast::Agent,
                              repast::RepastEdge<repast::Agent>,
                              repast::RepastEdgeContent<repast::Agent>,
                              repast::RepastEdgeContentManager<repast::Agent> >* AcceptedJobNetwork) {

  // get incoming wage payments
  std::vector<repast::Agent*> WagePayments;
  WagePaymentNetwork->adjacent(this, WagePayments);

  // start checking if everything is correct
  if(WagePayments.size() > 1) {
    std::cout << "Cons " << this->getId().id() << std::endl;
    throw std::runtime_error( "Too many wage receipts!");
  }
  if(WagePayments.size() == 1 and not isUnemployed) {
    std::vector<repast::Agent*> MyEmployer;
    AcceptedJobNetwork->adjacent(this, MyEmployer);
    if(MyEmployer.size() > 1) {
      throw std::runtime_error( "Cons keeps too many employers!");
    } else if(MyEmployer.size() == 1) {
      // check if wage receipt is coming from our employer
      if(WagePayments[0]->getId() == MyEmployer[0]->getId()) {
        // everything in order
      } else {
        throw std::runtime_error( "Wage receipt and employer are not the same!");
  }
    } else if(MyEmployer.size() == 0) {
      throw std::runtime_error( "Cons is employed but doesn't keep track of its employer!");
    }

    ReceivedWage = WagePaymentNetwork->findEdge(this, WagePayments[0])->weight();
    Cons_Liquidity += ReceivedWage;
  } else if(WagePayments.size() == 0 and isUnemployed){
    // consumer is unemployed
    ReceivedWage = 0;
  } else if(WagePayments.size() == 1 and isUnemployed) {
    this->writeScreen();
    throw std::runtime_error( "Cons is unemployed but received wages!");
  } else if(WagePayments.size() == 0 and not isUnemployed) {
    // std::cout << "XXX Cons " << this->getId().id() << " is employed but does not receive a wage!";
    // this->writeScreen();
    // throw std::runtime_error( "Cons is employed but does not receive a wage!");
  } else {
    throw std::runtime_error( "Whatever. Very weird");
  }
  // std::cout << "Cons " << this->getId().id() << " rec wage: "
  // << ReceivedWage;

  // get incoming dividend payments
  std::vector<repast::Agent*> DividendPayments;
  DividendPaymentNetwork->adjacent(this, DividendPayments);
  ReceivedDividend = 0;
  for(std::vector<repast::Agent*>::iterator it = DividendPayments.begin();
      it != DividendPayments.end(); it++) {
    ReceivedDividend += DividendPaymentNetwork->findEdge(this, *it)->weight();
  }
  Cons_Liquidity += ReceivedDividend;
  // std::cout << " & div: " << ReceivedDividend
  // << " (" << DividendPayments.size() << ") " << std::endl;

}

void Cons::adjReservationWage() {
  if(!isUnemployed and ReceivedWage >= ReservationWage) {
    ReservationWage = ReceivedWage;
  } else if(!isUnemployed and ReceivedWage < ReservationWage) {
    // intensify looking for a job
  } else if(isUnemployed) {
    // if I am still unemployed, reduce my reservation wage
    ReservationWage *= 0.9;
  }
  // save the realized demand during the month in a separate variable
  realizedDemand_lastMonth = realizedDemand;

}

void Cons::writeOut(std::ostream& out) {
  out << this->getId().id() << ","
      << this->getisUnemployed() << ","
      << this->getEmployerID() << ","
      << this->getCons_Liquidity() << ","
      << this->getReservationWage() << ","
      << this->getReceivedWage() << ","
      << this->getReceivedDividend() << ","
      << this->getplannedDemand() << ","
      << this->getdailyDemand() << ","
      << this->getrealizedDemand() << ","
      << this->getrealizedDemand_lastMonth() << ","
      << this->getunsatisfiedDemand() << ","
      << this->getmySellerFirms()
      << std::endl;
}

void Cons::writeScreen() {
  std::cout << this->getId().id() << ","
            << this->getisUnemployed() << ","
            << this->getEmployerID() << ","
            << this->getCons_Liquidity() << ","
            << this->getReservationWage() << ","
            << this->getReceivedWage() << ","
            << this->getReceivedDividend() << ","
            << this->getplannedDemand() << ","
            << this->getdailyDemand() << ","
            << this->getrealizedDemand() << ","
            << this->getrealizedDemand_lastMonth() << ","
            << this->getunsatisfiedDemand() << ","
            << this->getmySellerFirms()
            << std::endl;
}
