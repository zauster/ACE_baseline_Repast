#include <boost/mpi.hpp>
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"

#include "BLModel.hpp"


int main(int argc, char** argv){

  // The name of the configuration file is Arg 1
  std::string configFile = argv[1];
  // The name of the properties file is Arg 2
  std::string propsFile  = argv[2];
  boost::mpi::environment env(argc, argv);
  boost::mpi::communicator world;
  repast::RepastProcess::init(configFile);

  // start the timer
  double t1, t2;
  if(repast::RepastProcess::instance()->rank() == 0) {
    t1 = MPI_Wtime();
  }

  BLModel* model = new BLModel(propsFile, argc, argv, &world);
  repast::ScheduleRunner& runner = repast::RepastProcess::instance()->getScheduleRunner();

  // init model and distribute agents
  if(repast::RepastProcess::instance()->rank() == 0) {
    std::cout << std::endl
              << " => Initializing ..." << std::endl;
  }
  model->initStates();

  // carry the simulation out
  if(repast::RepastProcess::instance()->rank() == 0) {
    std::cout << std::endl
              << " => Starting the simulation ..." << std::endl;
  }
  model->initSchedule(runner);
  runner.run();

  if(repast::RepastProcess::instance()->rank() == 0) {
    t2 = MPI_Wtime();
  }

  if(repast::RepastProcess::instance()->rank() == 0) {
    std::ofstream logfile;
    logfile.open("output/log.csv");
    logfile << "Totaltime, NrProc, NrFirms, NrConsumers, NrIter, Application\n"
            << (t2 - t1) * 1000 << ", "
            << model->getWorldSize() << ", "
            << model->getNrFirms() << ", "
            << model->getNrConsumers() << ", "
            << model->getStopAt() << ", "
            << "Repast"
            << "\n";
    logfile.close();
  }

  if(repast::RepastProcess::instance()->rank() == 0) {
    std::cout << std::endl
              << " => Stopping the simulation ..." << std::endl;
  }
  delete model;
  repast::RepastProcess::instance()->done();
}
