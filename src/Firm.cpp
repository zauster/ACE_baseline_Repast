#include "Firm.hpp"

Firm::Firm(repast::AgentId id): Firm_ID(id), Firm_Liquidity(10),
                                currentInventories(20){ }

Firm::Firm(repast::AgentId _Firm_ID, // int _Firm_ID,
           double _Firm_Liquidity,
           double _marginalCosts,
           bool _openVacancy,
           bool _FireAnEmployee,
           int _OutstandingJobOffers,
           bool _openVacancy_lastMonth,
           int _MonthsSinceLastVacancy,
           double _currentWage,
           int _NrEmployees,
           double _currentInventories,
           double _currentDemand,
           double _currentPrice,
           std::vector<int> _EmployeeList
           ):
  // Firm_ID(repast::AgentId(_Firm_ID,
  //                         repast::RepastProcess::instance()->rank(),
  //                         1)),
  Firm_ID(_Firm_ID),
  Firm_Liquidity(_Firm_Liquidity),
  marginalCosts(_marginalCosts),
  openVacancy(_openVacancy),
  FireAnEmployee(_FireAnEmployee),
  OutstandingJobOffers(_OutstandingJobOffers),
  openVacancy_lastMonth(_openVacancy_lastMonth),
  MonthsSinceLastVacancy(_MonthsSinceLastVacancy),
  currentWage(_currentWage),
  NrEmployees(_NrEmployees),
  currentInventories(_currentInventories),
  currentDemand(_currentDemand),
  currentPrice(_currentPrice),
  EmployeeList(_EmployeeList) {}

Firm::~Firm(){ }

void Firm::set(int currentRank,
               double _Firm_Liquidity,
               double _marginalCosts,
               bool _openVacancy,
               bool _FireAnEmployee,
               int _OutstandingJobOffers,
               bool _openVacancy_lastMonth,
               int _MonthsSinceLastVacancy,
               double _currentWage,
               int _NrEmployees,
               double _currentInventories,
               double _currentDemand,
               double _currentPrice,
               std::vector<int> _EmployeeList){
  Firm_ID.currentRank(currentRank);
  Firm_Liquidity = _Firm_Liquidity;
  marginalCosts = _marginalCosts;
  openVacancy = _openVacancy;
  FireAnEmployee = _FireAnEmployee;
  OutstandingJobOffers = _OutstandingJobOffers;
  openVacancy_lastMonth = _openVacancy_lastMonth;
  MonthsSinceLastVacancy = _MonthsSinceLastVacancy;
  currentWage = _currentWage;
  NrEmployees = _NrEmployees;
  currentInventories = _currentInventories;
  currentDemand = _currentDemand;
  currentPrice = _currentPrice;
  EmployeeList = _EmployeeList;
}

void Firm::fireEmployee(repast::SharedNetwork<repast::Agent,
                        repast::RepastEdge<repast::Agent>,
                        repast::RepastEdgeContent<repast::Agent>,
                        repast::RepastEdgeContentManager<repast::Agent> >* LabourMarketNetwork) {
  std::vector<repast::Agent*> myWorkers;

  if(FireAnEmployee and not openVacancy) {
    LabourMarketNetwork->adjacent(this, myWorkers);
    int NrEdgesBefore = LabourMarketNetwork->outDegree(this);

    // get a random index, this will be the fired worker
    int ri = getRandIndex(myWorkers.size());

    Cons* firedWorker = dynamic_cast<Cons*>(myWorkers[ri]);
    LabourMarketNetwork->removeEdge(this, firedWorker);
    FireAnEmployee = false;
    NrEmployees--;
    int NrEdgesAfter = LabourMarketNetwork->outDegree(this);

#ifdef EMPVERBOSE
    std::cout << "Firm " << Firm_ID << " firing employee "
              << firedWorker->getId()
              << ".  " << NrEdgesBefore << " - " << NrEdgesAfter
              << std::endl;
#endif

  } else if(not openVacancy) {
    openVacancy_lastMonth++;
  } else if(FireAnEmployee and openVacancy) {
    FireAnEmployee = false;
    openVacancy = false;
  }

  // save the currently employed workers in a separate vector
  myWorkers.clear();
  EmployeeList.clear();
  LabourMarketNetwork->adjacent(this, myWorkers);
  for(int i = 0; i < myWorkers.size(); i++) {
    EmployeeList.push_back(myWorkers[i]->getId().id());
  }
}

void Firm::sendJobOffers(repast::SharedNetwork<repast::Agent,
                         repast::RepastEdge<repast::Agent>,
                         repast::RepastEdgeContent<repast::Agent>,
                         repast::RepastEdgeContentManager<repast::Agent> >* LabourSearchNetwork,
                         repast::SharedNetwork<repast::Agent,
                         repast::RepastEdge<repast::Agent>,
                         repast::RepastEdgeContent<repast::Agent>,
                         repast::RepastEdgeContentManager<repast::Agent> >* JobOfferNetwork
                         ) {
  // if they is an open vacancy, have a look at the applicants
  if(openVacancy) {
    std::vector<repast::Agent*> JobApplicants;
    LabourSearchNetwork->adjacent(this, JobApplicants);
    std::vector<repast::Agent*> ConsideredApplicants;

    // First, lets remove the job applicants that are requesting too
    // high wages
    if(JobApplicants.size() > 0) {
      for(std::vector<repast::Agent*>::iterator app_it = JobApplicants.begin(); app_it != JobApplicants.end(); app_it++) {

#ifdef EMPVERBOSE
        std::cout << "Firm " << this->getId().id() << ": cons "
                  << (*app_it)->getId().id() << " applying. "
                  << " CW: " << currentWage << " RW: "
                  << LabourSearchNetwork->findEdge(this, *app_it)->weight()
                  << std::endl;
#endif

        double requestedWage = LabourSearchNetwork->findEdge(this, *app_it)->weight();

        if(requestedWage < currentWage) {
          ConsideredApplicants.push_back(*app_it);
        }
      }
    }

    // if there are still applicants left, choose one randomly
    if(ConsideredApplicants.size() > 0) {

      int ri = 0;
      if(ConsideredApplicants.size() > 1) {
        ri = getRandIndex(ConsideredApplicants.size());
      }

#ifdef EMPVERBOSE
      std::cout << " -> Firm " << this->getId().id() << ": JO sent to index "
                << ri << " of " << ConsideredApplicants.size()
                << " is Cons " << ConsideredApplicants[ri]->getId().id()
                << std::endl;
#endif

      // add edge in the JobOfferNetwork
      JobOfferNetwork->addEdge(this, ConsideredApplicants[ri], currentWage);
      OutstandingJobOffers = 1;
    }
  }
}

void Firm::checkAcceptedJobOffers(repast::SharedNetwork<repast::Agent,
                                  repast::RepastEdge<repast::Agent>,
                                  repast::RepastEdgeContent<repast::Agent>,
                                  repast::RepastEdgeContentManager<repast::Agent> >* AcceptedJobNetwork,
                                  repast::SharedNetwork<repast::Agent,
                                  repast::RepastEdge<repast::Agent>,
                                  repast::RepastEdgeContent<repast::Agent>,
                                  repast::RepastEdgeContentManager<repast::Agent> >* LeaveJobNetwork,
                                  repast::SharedNetwork<repast::Agent,
                                  repast::RepastEdge<repast::Agent>,
                                  repast::RepastEdgeContent<repast::Agent>,
                                  repast::RepastEdgeContentManager<repast::Agent> >* LabourMarketNetwork) {

  // even if there was no openVacancy, there might be a notice of
  // leave of another consumer
  std::vector<repast::Agent*> myWorkers;
  LabourMarketNetwork->adjacent(this, myWorkers);
  std::vector<repast::Agent*> AcceptedJobs;
  AcceptedJobNetwork->adjacent(this, AcceptedJobs);
  std::vector<repast::Agent*> LeftWorkers;
  LeaveJobNetwork->adjacent(this, LeftWorkers);
  std::vector<repast::Agent*>::iterator acc_it, emp_it;
  Cons* tmpCons;
  bool newWorker = false;

  if(openVacancy) {
    if(AcceptedJobs.size() > 0) {
      // some workers have accepted our job offers
      for(acc_it = AcceptedJobs.begin();
          acc_it != AcceptedJobs.end(); acc_it++) {

        bool isEmployee = false;
        for(emp_it = myWorkers.begin();
            emp_it != myWorkers.end(); emp_it++) {
          if((*emp_it)->getId() == (*acc_it)->getId()) {
            isEmployee = true;
            break;
          }
        }

        // if we haven't found the cons in our list of employee, he
        // must be a new one and we add them to our list
        if(!isEmployee) {
          newWorker = true;

#ifdef EMPVERBOSE
          std::cout << "Firm " << this->getId().id() << " taking on cons "
                    << (*acc_it)->getId().id() << std::endl;
#endif

          tmpCons = dynamic_cast<Cons*> (*acc_it);
          LabourMarketNetwork->addEdge(this, tmpCons);
        }
      }
    }
  }

  if(LeftWorkers.size() > 0) {
    // some workers have decided to leave our firm, remove the edges to them
    std::vector<repast::Agent*>::iterator left_it;
    for(left_it = LeftWorkers.begin();
        left_it != LeftWorkers.end(); left_it++) {

#ifdef EMPVERBOSE
      std::cout << "Firm " << this->getId().id() << " loses cons "
                << (*left_it)->getId().id() << std::endl;
#endif

      tmpCons = dynamic_cast<Cons*> (*left_it);
      LabourMarketNetwork->removeEdge(this, tmpCons);
    }
  }

  // update the list of employees
  EmployeeList.clear();
  myWorkers.clear();
  LabourMarketNetwork->adjacent(this, myWorkers);

#ifdef EMPVERBOSE
  if(newWorker) {
    std::cout << "Firm " << this->getId().id() << ":  ";
    for(emp_it = myWorkers.begin(); emp_it != myWorkers.end(); emp_it++) {
      std::cout << (*emp_it)->getId().id() << ", ";
    }
    std::cout << std::endl;
  }
#endif

  for(int i = 0; i < myWorkers.size(); i++) {
    EmployeeList.push_back(myWorkers[i]->getId().id());
  }
  // keep the number of employees up to date
  NrEmployees = EmployeeList.size();
}


void Firm::updatePrices(double lowerBoundInventories,
                        double upperBoundInventories,
                        double lowerBoundPrice,
                        double upperBoundPrice,
                        double theta,
                        double ProbNewPrice) {
  double newPrice = currentPrice;
  // std::cout << "Firm " << this->getId().id();
  if(currentInventories > upperBoundInventories * currentDemand) {
    // std::cout << " too MUCH inv.";
    // firm has too much inventories, thus fires a worker
    if(NrEmployees > 0) {
      FireAnEmployee = true;
    }
    openVacancy = false;

    // furthermore, the firms considers lowering the price
    newPrice = currentPrice * (1 - getRandDouble(0.0, theta));
    if(newPrice < lowerBoundPrice * marginalCosts) {
      newPrice = lowerBoundPrice * marginalCosts;
    }
  } else if(currentInventories < lowerBoundInventories * currentDemand) {
    // std::cout << " too LITTLE inv.";
    // firm is low on inventories, thus tries to hire a new workers
    openVacancy = true;
    MonthsSinceLastVacancy = 0;

    // additionally, consider increasing the price
    newPrice = currentPrice * (1 + getRandDouble(0.0, theta));
    if(newPrice > upperBoundPrice * marginalCosts) {
      newPrice = upperBoundPrice * marginalCosts;
    }
  } else {
    // do nothing
  }

  // with a certain probability, the firm sets the new price
  if(getRandDouble(0, 1) <= ProbNewPrice) {
    // std::cout << " New price = " << newPrice << " (old: "
    //           << currentPrice << ")";
    currentPrice = newPrice;
  }
  // std::cout << std::endl;

  // if there is no new open vacancy, increase the months since the
  // last one
  if(!openVacancy) {
    MonthsSinceLastVacancy++;
  }
  currentDemand = 0.0;
}

void Firm::updateWages(int gamma, double delta) {
  // there was an open vacancy last month and there is still one this
  // month, so the firms increases to wage to attract more workers
  if(openVacancy_lastMonth and openVacancy) {
    // std::cout << "Firm " << this->getId().id() << " inc wage "
    //           << " from " << currentWage;
    currentWage *= (1 + getRandDouble(0.0, delta));
    // std::cout << " to " << currentWage << std::endl;
  }

  if(MonthsSinceLastVacancy >= gamma) {
    // std::cout << "Firm " << this->getId().id() << " dec wage "
    //           << " from " << currentWage;
    currentWage *= (1 - getRandDouble(0.0, delta));
    // std::cout << " to " << currentWage << std::endl;
  }
}


void Firm::produceGoods(double LabourProductivity) {
  // std::cout << this->getId() << ": ci = " << currentInventories;
  currentInventories += LabourProductivity * NrEmployees;
  // std::cout << "  new ci = " << currentInventories << std::endl;
}

void Firm::sendGoods(repast::SharedNetwork<repast::Agent,
                     repast::RepastEdge<repast::Agent>,
                     repast::RepastEdgeContent<repast::Agent>,
                     repast::RepastEdgeContentManager<repast::Agent> >* GoodsMarketNetwork,
                     repast::SharedNetwork<repast::Agent,
                     repast::RepastEdge<repast::Agent>,
                     repast::RepastEdgeContent<repast::Agent>,
                     repast::RepastEdgeContentManager<repast::Agent> >* GoodsDeliveryNetwork) {
  std::vector<repast::Agent*> GoodsRequests;
  std::vector<repast::Agent*>::iterator it;
  GoodsMarketNetwork->adjacent(this, GoodsRequests);
  double requestedAmount, deliveredAmount;

  // std::cout << "Firm " << this->getId().id() << ":  ";
  // for(it = GoodsRequests.begin(); it != GoodsRequests.end(); it++) {
  //   std::cout << " (" << (*it)->getId().id() << ": "
  //             << GoodsMarketNetwork->findEdge(this, *it)->weight() << ") ";
  // }
  // std::cout << std::endl;

  for(std::vector<repast::Agent*>::iterator it = GoodsRequests.begin();
      it != GoodsRequests.end(); it++) {
    requestedAmount = GoodsMarketNetwork->findEdge(this, *it)->weight();

    if(requestedAmount < 0) {
      requestedAmount = 0;
    }
    deliveredAmount = requestedAmount;
    if(requestedAmount > currentInventories) {
      deliveredAmount = currentInventories;
    }

    // deliver that amount to the customer
    GoodsDeliveryNetwork->addEdge(this, *it, deliveredAmount);

    // reduce inventories by the delivered amount
    currentInventories -= deliveredAmount;

    // but increase the liquidity of the firm
    Firm_Liquidity += deliveredAmount * currentPrice;

    // and increase the demand the firm has seen
    currentDemand += requestedAmount;

    if(requestedAmount < 0) {
      std::cout << "Firm " << this->getId().id()
                << "  -- cons " << (*it)->getId().id()
                << "  req: " << requestedAmount
                << "  del: " << deliveredAmount
                << "  inv: " << currentInventories
                << std::endl;
    }

  }
}

void Firm::payWages(repast::SharedNetwork<repast::Agent,
                    repast::RepastEdge<repast::Agent>,
                    repast::RepastEdgeContent<repast::Agent>,
                    repast::RepastEdgeContentManager<repast::Agent> >* WagePaymentNetwork,
                    repast::SharedNetwork<repast::Agent,
                    repast::RepastEdge<repast::Agent>,
                    repast::RepastEdgeContent<repast::Agent>,
                    repast::RepastEdgeContentManager<repast::Agent> >* LabourMarketNetwork) {
  if(Firm_Liquidity > 0.1) {
    std::vector<repast::Agent*> myWorkers;
    LabourMarketNetwork->adjacent(this, myWorkers);
    std::vector<repast::Agent*>::iterator emp_it;

    double wagebill = currentWage * NrEmployees;

    // std::cout << "Firm " << this->getId().id()
    //           << "  NrE: " << NrEmployees
    //           << "  cW: " << currentWage
    //           << "  wb: " << wagebill
    //           << "  liq: " << Firm_Liquidity
    //           << "  diff: " << Firm_Liquidity - wagebill
    //           << std::endl;

    if(wagebill < Firm_Liquidity) {
      for(emp_it = myWorkers.begin(); emp_it != myWorkers.end(); emp_it++) {
        WagePaymentNetwork->addEdge(this, *emp_it, currentWage);
      }
      Firm_Liquidity -= wagebill;
    } else {
      double wagecut = Firm_Liquidity / NrEmployees;
      for(emp_it = myWorkers.begin(); emp_it != myWorkers.end(); emp_it++) {
        WagePaymentNetwork->addEdge(this, *emp_it, wagecut);
      }
      Firm_Liquidity = 0;

      // we reduce the wage by certain fraction, hoping we will be
      // able to pay that wage next month
      currentWage *= 0.9;

      // if we can't pay the wage, we definitely won't take on a new
      // employee
      openVacancy = false;
    }
  }
}

void Firm::payDividend(repast::SharedNetwork<repast::Agent,
                       repast::RepastEdge<repast::Agent>,
                       repast::RepastEdgeContent<repast::Agent>,
                       repast::RepastEdgeContentManager<repast::Agent> >* DividendPaymentNetwork,
                       std::vector<repast::Agent*> consumers) {

  double Savings = currentWage * NrEmployees * 0.1;
  double DividendTotal = Firm_Liquidity - Savings;

  // if the rest is still positive, we'll distribute it to the consumers
  if(DividendTotal > 0.1) {
    int highestID = EmployeeList[0];
    for(int i = 1; i < EmployeeList.size(); i++) {
      if(EmployeeList[i] > highestID) {
        highestID = EmployeeList[i];
      }
    }

    double Dividend = DividendTotal / highestID;
    int i = 0;
    std::vector<repast::Agent*>::iterator cons_it;
    for(cons_it = consumers.begin(); cons_it != consumers.end(); cons_it++) {
      if((*cons_it)->getId().id() <= highestID) {
        DividendPaymentNetwork->addEdge(this, *cons_it, Dividend);
        i++;
      }
    }
    if(i != highestID) {
      std::cout << "Firm " << this->getId().id()
                << "  csize: " << consumers.size()
                << "  i: " << i
                << "  high: " << highestID
                << std::endl;
      throw std::runtime_error( "Different number of dividend recipients!");
    }
    Firm_Liquidity -= DividendTotal;

    // std::cout << "Firm " << this->getId().id()
    //           << "  div: " << Dividend
    //           << "  sav: " << Savings
    //           // << "  high: " << highestID
    //           // << "  i: " << i
    //           << "  tot: " << DividendTotal
    //           << "  liq: " << Firm_Liquidity + DividendTotal
    //           << "  diff: " << Firm_Liquidity
    //           << std::endl;

  }
}

void Firm::writeOut (std::ostream& out) {
  out << this->getId().id()
      << "," << this->getFirm_Liquidity()
      << "," << this->getmarginalCosts()
      << "," << this->getopenVacancy()
      << "," << this->getFireAnEmployee()
      << "," << this->getOutstandingJobOffers()
      << "," << this->getopenVacancy_lastMonth()
      << "," << this->getMonthsSinceLastVacancy()
      << "," << this->getcurrentWage()
      << "," << this->getNrEmployees()
      << "," << this->getcurrentInventories()
      << "," << this->getcurrentDemand()
      << "," << this->getcurrentPrice()
      << "," << this->getEmployeeList_string()
      << std::endl;
}
