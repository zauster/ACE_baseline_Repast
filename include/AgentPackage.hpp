#ifndef AGENT_PACKAGE
#define AGENT_PACKAGE

#include <boost/mpi.hpp>
#include "repast_hpc/Schedule.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/AgentRequest.h"
// #include "repast_hpc/TDataSource.h"
// #include "repast_hpc/SVDataSet.h"
#include "repast_hpc/SharedNetwork.h"

#include "Firm.hpp"
#include "Cons.hpp"

/* Serializable Agent Package */
struct AgentPackage {

public:
  int id;
  int rank;
  int type;
  int currentRank;

  // Firm variables
  double Firm_Liquidity;
  double marginalCosts;
  bool openVacancy;
  bool FireAnEmployee;
  int OutstandingJobOffers;
  bool openVacancy_lastMonth;
  int MonthsSinceLastVacancy;
  double currentWage;
  int NrEmployees;
  double currentInventories;
  double currentDemand;
  double currentPrice;
  std::vector<int> EmployeeList;

  // Consumer variables
  double Cons_Liquidity;
  bool isUnemployed;
  int EmployerID;
  double ReservationWage;
  double ReceivedWage;
  double ReceivedDividend;
  double plannedDemand;
  double dailyDemand;
  double realizedDemand;
  double realizedDemand_lastMonth;
  double unsatisfiedDemand;
  // std::vector<int> mySellerFirms;
  std::map<int, double> mySellerFirms_currPrices;


  /* Constructors */
  AgentPackage(); // For serialization
  // Firm variables
  AgentPackage(int _id, int _rank, int _type, int _currentRank,
               double _Firm_Liquidity,
               double _marginalCosts,
               bool _openVacancy,
               bool _FireAnEmployee,
               int _OutstandingJobOffers,
               bool _openVacancy_lastMonth,
               int _MonthsSinceLastVacancy,
               double _currentWage,
               int _NrEmployees,
               double _currentInventories,
               double _currentDemand,
               double _currentPrice,
               std::vector<int> _EmployeeList);
  // Consumer variables
  AgentPackage(int _id, int _rank, int _type, int _currentRank,
               double _Cons_Liquidity,
               bool _isUnemployed,
               int _EmployerID,
               double _ReservationWage,
               double _ReceivedWage,
               double _ReceivedDividend,
               double _plannedDemand,
               double _dailyDemand,
               double _realizedDemand,
               double _realizedDemand_lastMonth,
               double _unsatisfiedDemand,
               // std::vector<int> _mySellerFirms,
               std::map<int, double> _mySellerFirms_currPrices);

  /* For archive packaging */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version){
    ar & id;
    ar & rank;
    ar & type;
    ar & currentRank;
    if(type == 1) {// it is a Firm
      ar & Firm_Liquidity;
      ar & marginalCosts;
      ar & openVacancy;
      ar & FireAnEmployee;
      ar & OutstandingJobOffers;
      ar & openVacancy_lastMonth;
      ar & MonthsSinceLastVacancy;
      ar & currentWage;
      ar & NrEmployees;
      ar & currentInventories;
      ar & currentDemand;
      ar & currentPrice;
      ar & EmployeeList;
    } else if(type == 2) { // it is a consumer
      ar & Cons_Liquidity;
      ar & isUnemployed;
      ar & EmployerID;
      ar & ReservationWage;
      ar & ReceivedWage;
      ar & ReceivedDividend;
      ar & plannedDemand;
      ar & dailyDemand;
      ar & realizedDemand;
      ar & realizedDemand_lastMonth;
      ar & unsatisfiedDemand;
      // ar & mySellerFirms;
      ar & mySellerFirms_currPrices;
    }
  }
};


/* Agent Package Provider */
class AgentPackageProvider {

private:
  repast::SharedContext<repast::Agent>* agents;

public:
  AgentPackageProvider(repast::SharedContext<repast::Agent>* agentPtr);
  void providePackage(repast::Agent* agent, std::vector<AgentPackage>& out);
  void provideContent(repast::AgentRequest req, std::vector<AgentPackage>& out);
};

/* Agent Package Receiver */
class AgentPackageReceiver {

private:
  repast::SharedContext<repast::Agent>* agents;

public:
  AgentPackageReceiver(repast::SharedContext<repast::Agent>* agentPtr);
  repast::Agent* createAgent(AgentPackage package);
  void updateAgent(AgentPackage package);
};
#endif
