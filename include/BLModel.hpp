#ifndef BL_MODEL
#define BL_MODEL

#include <boost/mpi.hpp>
#include <map>
#include <string>
#include "repast_hpc/Schedule.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/AgentRequest.h"
// #include "repast_hpc/TDataSource.h"
// #include "repast_hpc/SVDataSet.h"
#include "repast_hpc/SharedNetwork.h"

#include "Firm.hpp"
#include "Cons.hpp"
#include "AgentPackage.hpp"

class BLModel {
public:
  int stopAt;
  int NrFirms;
  int NrConsumers;
  int maxTypeA;
  double lowerBoundInventories;
  double upperBoundInventories;
  double lowerBoundPrice;
  double upperBoundPrice;
  double ProbNewPrice;
  int gamma;
  double delta;
  double theta;
  double pi;
  double epsilon;
  int maxNrFirms_openVacancy;
  int maxNrFirms_buyGoods;
  int NrFirms_PriceSwitch;
  double LabourProductivity;

  int worldSize;

  repast::Properties* props;
  repast::SharedContext<repast::Agent> context;

  AgentPackageProvider* agent_provider;
  AgentPackageReceiver* agent_receiver;

  repast::RepastEdgeContentManager<repast::Agent> edgeContentManager;

  repast::SharedNetwork<repast::Agent, repast::RepastEdge<repast::Agent>,
                        repast::RepastEdgeContent<repast::Agent>,
                        repast::RepastEdgeContentManager<repast::Agent> >* LabourMarketNetwork;
  repast::SharedNetwork<repast::Agent, repast::RepastEdge<repast::Agent>,
                        repast::RepastEdgeContent<repast::Agent>,
                        repast::RepastEdgeContentManager<repast::Agent> >* LabourSearchNetwork;
  repast::SharedNetwork<repast::Agent, repast::RepastEdge<repast::Agent>,
                        repast::RepastEdgeContent<repast::Agent>,
                        repast::RepastEdgeContentManager<repast::Agent> >* JobOfferNetwork;
  repast::SharedNetwork<repast::Agent, repast::RepastEdge<repast::Agent>,
                        repast::RepastEdgeContent<repast::Agent>,
                        repast::RepastEdgeContentManager<repast::Agent> >* AcceptedJobNetwork;
  repast::SharedNetwork<repast::Agent, repast::RepastEdge<repast::Agent>,
                        repast::RepastEdgeContent<repast::Agent>,
                        repast::RepastEdgeContentManager<repast::Agent> >* LeaveJobNetwork;


  // GoodsMarketNetwork
  repast::SharedNetwork<repast::Agent, repast::RepastEdge<repast::Agent>,
                        repast::RepastEdgeContent<repast::Agent>,
                        repast::RepastEdgeContentManager<repast::Agent> >* GoodsMarketNetwork;
  repast::SharedNetwork<repast::Agent, repast::RepastEdge<repast::Agent>,
                        repast::RepastEdgeContent<repast::Agent>,
                        repast::RepastEdgeContentManager<repast::Agent> >* GoodsDeliveryNetwork;


  // Payment Networks
  repast::SharedNetwork<repast::Agent, repast::RepastEdge<repast::Agent>,
                        repast::RepastEdgeContent<repast::Agent>,
                        repast::RepastEdgeContentManager<repast::Agent> >* WagePaymentNetwork;
  repast::SharedNetwork<repast::Agent, repast::RepastEdge<repast::Agent>,
                        repast::RepastEdgeContent<repast::Agent>,
                        repast::RepastEdgeContentManager<repast::Agent> >* DividendPaymentNetwork;


  BLModel(std::string propsFile, int argc, char** argv,
          boost::mpi::communicator* comm);
  ~BLModel();

  int getWorldSize() {
    return worldSize;
  }
  int getNrFirms() {
    return NrFirms;
  }
  int getNrConsumers() {
    return NrConsumers;
  }
  int getStopAt() {
    return stopAt;
  }
  void initStates();
  void initSchedule(repast::ScheduleRunner& runner);
  void doEmploymentUpdate();

  void updateTradingPartners();
  void doConsumerPricesUpdate();
  void doFirmsPricesUpdate();

  // daily routines
  void FirmsProduceGoods();
  void ConsumerConsumeGoods();

  void doWagePayments();

  void updateProjection();
  void writeAgentStates();

};

#endif
