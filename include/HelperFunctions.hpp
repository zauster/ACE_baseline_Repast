#include <stdio.h>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <random>
#include <iterator>

#include "repast_hpc/AgentId.h"
#include "repast_hpc/Random.h"

// function to split a string into strings
std::vector<std::string> getNextLineAndSplitIntoTokens(std::istream& str,
                                                       char split);

// template<typename Iter, typename RandomGenerator>
// Iter select_randomly(Iter start, Iter end, RandomGenerator& g);

// template<typename Iter>
// Iter select_randomly(Iter start, Iter end);

// repast::Agent select_randomly(repast::Agent start, repast::Agent end,
                              // RandomGenerator& g);
// repast::Agent select_randomly(repast::Agent start, repast::Agent end);

int getRandIndex(int nrElements);
int getRandInt(int lower, int upper);
double getRandDouble(double lower, double upper);
