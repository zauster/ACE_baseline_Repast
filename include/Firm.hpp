#ifndef FIRM_AGENT
#define FIRM_AGENT

#include <iostream>

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedNetwork.h"

#include "HelperFunctions.hpp"
#include "Cons.hpp"


class Firm : public repast::Agent {

private:
  repast::AgentId Firm_ID;
  double Firm_Liquidity;
  double marginalCosts;
  bool openVacancy;
  bool FireAnEmployee;
  int OutstandingJobOffers;
  bool openVacancy_lastMonth;
  int MonthsSinceLastVacancy;
  double currentWage;
  int NrEmployees;
  double currentInventories;
  double currentDemand;
  double currentPrice;
  std::vector<int> EmployeeList;

public:
  Firm(repast::AgentId id);
  Firm(){}
  Firm(repast::AgentId _Firm_ID, //int _Firm_ID,
       double _Firm_Liquidity,
       double _marginalCosts, bool _OpenVacancy, bool _FireAnEmployee,
       int _OutstandingJobOffers,
       bool _openVacancy_lastMonth,
       int _MonthsSinceLastVacancy,
       double _currentWage,
       int _NrEmployees,
       double _currentInventories,
       double _currentDemand,
       double _currentPrice,
       std::vector<int> _EmployeeList);
  // Firm(std::vector<std::string> rowInput);

  ~Firm();

  /* Required Getters */
  repast::AgentId& getId() {
    return Firm_ID;
  }
  const repast::AgentId& getId() const {
    return Firm_ID;
  }

  /* Getters specific to this kind of Agent */
  double getcurrentPrice() {
    return currentPrice;
  }

  double getcurrentInventories() {
    return currentInventories;
  }
  double getFirm_Liquidity() {
    return Firm_Liquidity;
  }
  double getmarginalCosts() {
    return marginalCosts;
  }
  bool getopenVacancy() {
    return openVacancy;
  }
  bool getFireAnEmployee() {
    return FireAnEmployee;
  }
  int getOutstandingJobOffers() {
    return OutstandingJobOffers;
  }
  bool getopenVacancy_lastMonth() {
    return openVacancy_lastMonth;
  }
  int getMonthsSinceLastVacancy() {
    return MonthsSinceLastVacancy;
  }
  double getcurrentWage() {
    return currentWage;
  }
  int getNrEmployees() {
    return NrEmployees;
  }
  double getcurrentDemand() {
    return currentDemand;
  }
  std::vector<int> getEmployeeList() {
    return EmployeeList;
  }
  std::string getEmployeeList_string() {
    std::string out = "{";
    for(std::vector<int>::iterator it = EmployeeList.begin();
        it != EmployeeList.end(); it++) {
      out += std::to_string(*it);
      out += ":";
    }
    out.pop_back();
    out += "}";
    return out;
  }

  /* Setter */
  void set(int currentRank,
           double _Firm_Liquidity,
           double _marginalCosts,
           bool _openVacancy,
           bool _FireAnEmployee,
           int _OutstandingJobOffers,
           bool _openVacancy_lastMonth,
           int _MonthsSinceLastVacancy,
           double _currentWage,
           int _NrEmployees,
           double _currentInventories,
           double _currentDemand,
           double _currentPrice,
           std::vector<int> _EmployeeList);

  /* Actions */

  // fire an employee
  void fireEmployee(repast::SharedNetwork<repast::Agent,
                    repast::RepastEdge<repast::Agent>,
                    repast::RepastEdgeContent<repast::Agent>,
                    repast::RepastEdgeContentManager<repast::Agent> >* LabourMarketNetwork);
  void sendJobOffers(repast::SharedNetwork<repast::Agent,
                     repast::RepastEdge<repast::Agent>,
                     repast::RepastEdgeContent<repast::Agent>,
                     repast::RepastEdgeContentManager<repast::Agent> >* LabourSearchNetwork,
                     repast::SharedNetwork<repast::Agent,
                     repast::RepastEdge<repast::Agent>,
                     repast::RepastEdgeContent<repast::Agent>,
                     repast::RepastEdgeContentManager<repast::Agent> >* JobOfferNetwork);
  void checkAcceptedJobOffers(repast::SharedNetwork<repast::Agent,
                              repast::RepastEdge<repast::Agent>,
                              repast::RepastEdgeContent<repast::Agent>,
                              repast::RepastEdgeContentManager<repast::Agent> >* AcceptedJobNetwork, repast::SharedNetwork<repast::Agent,
                              repast::RepastEdge<repast::Agent>,
                              repast::RepastEdgeContent<repast::Agent>,
                              repast::RepastEdgeContentManager<repast::Agent> >* LeaveJobNetwork, repast::SharedNetwork<repast::Agent,
                              repast::RepastEdge<repast::Agent>,
                              repast::RepastEdgeContent<repast::Agent>,
                              repast::RepastEdgeContentManager<repast::Agent> >* LabourMarketNetwork);

  void updatePrices(double, double, double, double, double, double);
  void updateWages(int, double);

  void produceGoods(double LabourProductivity);
  void sendGoods(repast::SharedNetwork<repast::Agent,
                 repast::RepastEdge<repast::Agent>,
                 repast::RepastEdgeContent<repast::Agent>,
                 repast::RepastEdgeContentManager<repast::Agent> >* GoodsMarketNetwork,
                 repast::SharedNetwork<repast::Agent,
                 repast::RepastEdge<repast::Agent>,
                 repast::RepastEdgeContent<repast::Agent>,
                 repast::RepastEdgeContentManager<repast::Agent> >* GoodsDeliveryNetwork);

  void payWages(repast::SharedNetwork<repast::Agent,
                repast::RepastEdge<repast::Agent>,
                repast::RepastEdgeContent<repast::Agent>,
                repast::RepastEdgeContentManager<repast::Agent> >* WagePaymentNetwork,
                repast::SharedNetwork<repast::Agent,
                repast::RepastEdge<repast::Agent>,
                repast::RepastEdgeContent<repast::Agent>,
                repast::RepastEdgeContentManager<repast::Agent> >* LabourMarketNetwork);

  void payDividend(repast::SharedNetwork<repast::Agent,
                   repast::RepastEdge<repast::Agent>,
                   repast::RepastEdgeContent<repast::Agent>,
                   repast::RepastEdgeContentManager<repast::Agent> >* DividendPaymentNetwork,
                   std::vector<repast::Agent*> consumers);

  void writeOut (std::ostream& out);
};

#endif
