#ifndef CONSUMER_AGENT
#define CONSUMER_AGENT

#include <map>
#include <iostream>
#include <string>
#include <assert.h>
#include <stdexcept>

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedNetwork.h"
#include "HelperFunctions.hpp"
#include "Firm.hpp"

class Cons : public repast::Agent {

private:
  repast::AgentId Cons_ID;
  bool isUnemployed;
  int EmployerID;
  double Cons_Liquidity;
  double ReservationWage;
  double ReceivedWage;
  double ReceivedDividend;
  double plannedDemand;
  double dailyDemand;
  double realizedDemand;
  double realizedDemand_lastMonth;
  double unsatisfiedDemand;
  std::map<int, double> mySellerFirms_currPrices;

public:
  Cons(repast::AgentId id);
  Cons(){}
  Cons(repast::AgentId Cons_ID, // int Cons_ID,
       bool isUnemployed,
       int EmployerID,
       double Cons_Liquidity,
       double ReservationWage,
       double ReceivedWage,
       double ReceivedDividend,
       double plannedDemand,
       double dailyDemand,
       double realizedDemand,
       double realizedDemand_lastMonth,
       double unsatisfiedDemand,
       // std::vector<int> mySellerFirms,
       std::map<int, double> mySellerFirms_currPrices);
  Cons(std::vector<std::string> rowInput);

  ~Cons();

  /* Required Getters */
  repast::AgentId& getId() {
    return Cons_ID;
  }
  const repast::AgentId& getId() const {
    return Cons_ID;
  }

  /* Getters specific to this kind of Agent */
  double getCons_Liquidity() {
    return Cons_Liquidity;
  }
  bool getisUnemployed() {
    return isUnemployed;
  }
  int getEmployerID() {
    return EmployerID;
  }
  double getReservationWage() {
    return ReservationWage;
  }
  double getReceivedWage() {
    return ReceivedWage;
  }
  double getReceivedDividend() {
    return ReceivedDividend;
  }
  double getplannedDemand() {
    return plannedDemand;
  }
  double getdailyDemand() {
    return dailyDemand;
  }
  double getrealizedDemand() {
    return realizedDemand;
  }
  double getrealizedDemand_lastMonth() {
    return realizedDemand_lastMonth;
  }
  double getunsatisfiedDemand() {
    return unsatisfiedDemand;
  }
  // std::vector<int> getmySellerFirms() {
  // return mySellerFirms;
  // }
  std::map<int, double> getmySellerFirms_currPrices() {
    return mySellerFirms_currPrices;
  }
  std::string getmySellerFirms() {
    std::string out = "{";
    for(std::map<int, double>::iterator it = mySellerFirms_currPrices.begin();
        it != mySellerFirms_currPrices.end(); it++) {
      out += std::to_string(it->first);
      out += ":";
    }
    out.pop_back();
    out += "}";
    return out;
  }


  /* Setter */
  void set(int currentRank,
           bool _isUnemployed,
           int _EmployerID,
           double _Cons_Liquidity,
           double _ReservationWage,
           double _ReceivedWage,
           double _ReceivedDividend,
           double _plannedDemand,
           double _dailyDemand,
           double _realizedDemand,
           double _realizedDemand_lastMonth,
           double _unsatisfiedDemand,
           // std::vector<int> _mySellerFirms,
           std::map<int, double> _mySellerFirms_currPrices);

  /* Actions */
  void checkEmployment(repast::SharedNetwork<repast::Agent,
                       repast::RepastEdge<repast::Agent>,
                       repast::RepastEdgeContent<repast::Agent>,
                       repast::RepastEdgeContentManager<repast::Agent> >* AcceptedJobNetwork, repast::SharedNetwork<repast::Agent,
                       repast::RepastEdge<repast::Agent>,
                       repast::RepastEdgeContent<repast::Agent>,
                       repast::RepastEdgeContentManager<repast::Agent> >* LabourMarketNetwork);
  void findNewWork(repast::SharedNetwork<repast::Agent,
                   repast::RepastEdge<repast::Agent>,
                   repast::RepastEdgeContent<repast::Agent>,
                   repast::RepastEdgeContentManager<repast::Agent> >* LabourMarketNetwork,
                   std::vector<repast::Agent*> firms,
                   int NrFirms, int NrProc,
                   int maxNrFirms_openVacancy);
  void checkJobOffers(repast::SharedNetwork<repast::Agent,
                      repast::RepastEdge<repast::Agent>,
                      repast::RepastEdgeContent<repast::Agent>,
                      repast::RepastEdgeContentManager<repast::Agent> >* JobOfferNetwork, repast::SharedNetwork<repast::Agent,
                      repast::RepastEdge<repast::Agent>,
                      repast::RepastEdgeContent<repast::Agent>,
                      repast::RepastEdgeContentManager<repast::Agent> >* LeaveJobNetwork,repast::SharedNetwork<repast::Agent,
                      repast::RepastEdge<repast::Agent>,
                      repast::RepastEdgeContent<repast::Agent>,
                      repast::RepastEdgeContentManager<repast::Agent> >* LabourMarketNetwork);

  void updateTradingPartners(repast::SharedNetwork<repast::Agent,
                             repast::RepastEdge<repast::Agent>,
                             repast::RepastEdgeContent<repast::Agent>,
                             repast::RepastEdgeContentManager<repast::Agent> >* GoodsMarketNetwork,
                             int NrFirms_PriceSwitch,
                             int NrProc,
                             std::vector<repast::Agent*> firms);

  void updatePrices(repast::SharedNetwork<repast::Agent,
                    repast::RepastEdge<repast::Agent>,
                    repast::RepastEdgeContent<repast::Agent>,
                    repast::RepastEdgeContentManager<repast::Agent> >* GoodsMarketNetwork, double epsilon);


  void sendOrder(int maxNrFirms_buyGoods,
                 repast::SharedNetwork<repast::Agent,
                 repast::RepastEdge<repast::Agent>,
                 repast::RepastEdgeContent<repast::Agent>,
                 repast::RepastEdgeContentManager<repast::Agent> >* GoodsMarketNetwork);
  void consumeGoods(repast::SharedNetwork<repast::Agent,
                    repast::RepastEdge<repast::Agent>,
                    repast::RepastEdgeContent<repast::Agent>,
                    repast::RepastEdgeContentManager<repast::Agent> >* GoodsDeliveryNetwork);

  void recordWagePayments(repast::SharedNetwork<repast::Agent,
                          repast::RepastEdge<repast::Agent>,
                          repast::RepastEdgeContent<repast::Agent>,
                          repast::RepastEdgeContentManager<repast::Agent> >* WagePaymentNetwork,
                          repast::SharedNetwork<repast::Agent,
                          repast::RepastEdge<repast::Agent>,
                          repast::RepastEdgeContent<repast::Agent>,
                          repast::RepastEdgeContentManager<repast::Agent> >* DividendPaymentNetwork,
                          repast::SharedNetwork<repast::Agent,
                          repast::RepastEdge<repast::Agent>,
                          repast::RepastEdgeContent<repast::Agent>,
                          repast::RepastEdgeContentManager<repast::Agent> >* AcceptedJobNetwork);

  void adjReservationWage();

  void writeOut (std::ostream& out);
  void writeScreen();

};

#endif
